@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Register</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('inviter_email') ? ' has-error' : '' }}">
                                <label for="inviter_id" class="col-md-4 control-label">Inviter Email</label>

                                <div class="col-md-6">
                                    <input id="inviter_email"
                                           type="text"
                                           class="form-control"
                                           name="inviter_email"
                                           value="{{ old('inviter_email') }}">

                                    @if ($errors->has('inviter_email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('inviter_email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('inviter_name') ? ' has-error' : '' }}">
                                <label for="inviter_name" class="col-md-4 control-label">Inviter First Name</label>

                                <div class="col-md-6">
                                    <input id="inviter_name"
                                           type="text"
                                           class="form-control"
                                           name="inviter_name"
                                           value="{{ old('inviter_name') }}">

                                    @if ($errors->has('inviter_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('inviter_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('inviter_birthed') ? ' has-error' : '' }}">
                                <label for="inviter_birthed" class="col-md-4 control-label">Inviter Date of Birth</label>

                                <div class="col-md-6">
                                    <input id="inviter_birthed"
                                           type="date"
                                           class="form-control"
                                           name="inviter_birthed"
                                           value="{{ old('inviter_birthed') }}">

                                    @if ($errors->has('date_birthed'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('inviter_birthed') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('name_given') ? ' has-error' : '' }}">
                                <label for="name_given" class="col-md-4 control-label">First Name</label>

                                <div class="col-md-6">
                                    <input id="name_given"
                                           type="text"
                                           class="form-control"
                                           name="name_given"
                                           value="{{ old('name_given') }}">

                                    @if ($errors->has('name_given'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name_given') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('name_family') ? ' has-error' : '' }}">
                                <label for="name_family" class="col-md-4 control-label">Last Name</label>

                                <div class="col-md-6">
                                    <input id="name_family"
                                           type="text"
                                           class="form-control"
                                           name="name_family"
                                           value="{{ old('name_family') }}">

                                    @if ($errors->has('name_family'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name_family') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Email</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password"
                                           type="password"
                                           class="form-control"
                                           name="password"
                                           value="{{ old('password') }}">

                                    @if ($errors->has('password'))<span class="
                                           help-block">
                                    <strong>{{ $errors->first('password') }}</strong></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input id="password-confirm"
                                           type="password"
                                           class="form-control"
                                           name="password_confirmation"
                                           value="{{ old('password_confirmation') }}">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="
                                           help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('date_birthed') ? ' has-error' : '' }}">
                                <label for="date_birthed" class="col-md-4 control-label">Date of Birth</label>

                                <div class="col-md-6">
                                    <input id="date_birthed"
                                           type="date"
                                           class="form-control"
                                           name="date_birthed"
                                           value="{{ old('date_birthed') }}">

                                    @if ($errors->has('date_birthed'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('date_birthed') }}</strong></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                <label for="gender" class="col-md-4 control-label">Gender</label>

                                <div class="col-md-3">
                                    <input id="female"
                                           type="radio"
                                           class="form-control"
                                           name="gender"
                                           value="female" {{ old('gender')=="female" ? 'checked='.'"'.'checked'.'"' : '' }}>
                                    <label for="female"><i class="fa fa-female" aria-hidden="true"></i> Female</label>
                                </div>

                                <div class="col-md-3">
                                    <input id="male"
                                           type="radio"
                                           class="form-control"
                                           name="gender"
                                           value="male" {{ old('gender')=="male" ? 'checked='.'"'.'checked'.'"' : '' }}>
                                    <label for="male"><i class="fa fa-male" aria-hidden="true"></i> Male</label>
                                </div>

                                @if ($errors->has('gender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                @endif

                            </div>

                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="address" class="col-md-4 control-label">Address</label>

                                <div class="col-md-6">
                                    <input id="address"
                                           type="text"
                                           class="form-control"
                                           name="address"
                                           value="{{ old('address') }}">

                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <script src="https://checkout.stripe.com/checkout.js"
                                            class="stripe-button"
                                            data-key="pk_test_rDWgdrAFg8VRFxxQqsDG8YDm"
                                            data-amount="1999"
                                            data-name="Yearly Subscription"
                                            data-description="Yearly, auto-renewing subscription"
                                            data-image="https://s3.amazonaws.com/stripe-uploads/acct_18UzosIGnxKmmQPumerchant-icon-1470679238763-powon.png"
                                            data-locale="auto"
                                            data-currency="cad"></script>
                                    {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--<i class="fa fa-btn fa-user"></i> Register--}}
                                    {{--</button>--}}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
