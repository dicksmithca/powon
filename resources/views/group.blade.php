<?php
// a few variables for the publish_post include
$trigger_text = 'Add a post';
$modal_title  = 'Add post';
$url          = '/post/' . $group->id;
$button_class = 'btn btn-primary btn-xs pull-right';
$i            = 0;
$top_post     = true;
?>

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!--Moved code out of here as it is re-usable by the (other) user_profile display user.blade.php-->
                @include('includes.group_column')
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-heading">{{$group->name}}</div>

                        <div class="panel-body">

                            <div class="col-md-10">
                                <p>{{$group->description}}</p>
                                <p>Group owner: {{$owner->name_given}} {{$owner->name_family}}</p>
                            </div>
                            @if(Auth::user()->status === 'active' && (Auth::user()->id === $group->created_by || Auth::user()->roles === 'admin' ))
                                <div class="col-md-2">
                                    <!-- Trigger the modal with a button -->
                                    <div class="col-md-3">
                                        <button type="button"
                                                class="btn btn-info btn-sm"
                                                data-toggle="modal"
                                                data-target="#UpModal">Update
                                        </button>
                                        <br> <br>
                                        <button type="button"
                                                class="btn btn-info btn-sm"
                                                data-toggle="modal"
                                                data-target="#AddUserModal">Add Users!
                                        </button>
                                    </div>
                                    <div id="UpModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Update Group</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{url('update/group/'.$group->id)}}" method="POST">
                                                        {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <label for="group-nameupdate" class="control-label">New Group Name*
                                                            </label>
                                                            <input type="text"
                                                                   name="group-nameupdate"
                                                                   id="group-nameupdate"
                                                                   class="form-control"
                                                                   value="{{$group->name}}" />
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="group-bodyupdate" class="control-label">New Group
                                                                                                                Description*
                                                            </label>
                                                            <textarea name="group-bodyupdate"
                                                                      id="group-bodyupdate"
                                                                      class="form-control"
                                                                      >{{$group->description}}</textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="sel1">Select the group's interests</label>
                                                            <select multiple="multiple"
                                                                    size="5"
                                                                    class="form-control"
                                                                    id="select-interest"
                                                                    name="interests_update[]">
                                                                @foreach($interests as $interest)
                                                                    <option value="{{$interest->id}}">{{$interest->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-info btn-sm">
                                                                Update
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    @include('includes.add_user')

                                </div>
                            @endif
                        </div>
                    </div>
                    @if($is_member)
                        <div class="panel panel-default">
                            <div class="panel-heading">Posts
                                @include('includes.publish_post', ['post' => NULL,
                                                                    'top_post' => true,
                                                                    'edit' => false])
                            </div>
                            <div class="panel-body">

                                @if (count($posts) > 0)
                                    {{$top_post = false}}
                                    @foreach ($posts as $post)
                                        @include('partials.posts', ['post' => $post,
                                                                    'propagate' => true])
                                    @endforeach
                                @else
                                    <p>There are no posts to display!</p>
                                @endif
                            </div>

                        </div>
                    @endif
                </div>
            </div>
        </div>
@endsection
