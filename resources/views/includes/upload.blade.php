@if(!isset($post) || !($content = $post->has_content()))
    <div style="float: left">
        <div class="panel panel-default">
            <div class="panel panel-heading">Select image to upload:</div>
            <div class="panel panel-body">
                <div class="form-group-sm">
                    <input type="file" name="photo" id="photo" class="file">
                    <br>&nbsp;<br>
                    <label for="label">Enter a caption
                        <input type="text" name="label" class="form-control">
                    </label>
                </div>
            </div>
        </div>
    </div>
@endif