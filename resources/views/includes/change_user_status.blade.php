<!--{{$is_admin = (Auth::user()->roles === 'admin')}}-->
@if($is_admin && Auth::user()->id !== $user->id && Auth::user()->status === 'active')
    <?php
    $activate = array( 'type'      => 'activate',
                       'btn_class' => 'btn-success',
                       'label'     => 'Activate',
                       'icon'      => 'smile-o' );
    $deactivate = array( 'type'      => 'deactivate',
                         'btn_class' => 'btn-warning',
                         'label'     => 'Make inactive',
                         'icon'      => 'meh-o' );
    $suspend = array( 'type'      => 'suspend',
                      'btn_class' => 'btn-danger',
                      'label'     => 'Suspend',
                      'icon'      => 'frown-o' );
    if ( $user->status === 'active' ) {
        $update_status = $deactivate;
        $panel_type    = 'default';
    } elseif ( $user->status === 'inactive' ) {
        $update_status = $suspend;
        $panel_type    = 'warning';
    } else {
        $update_status = $activate;
        $panel_type    = 'danger';
    }
    ?>
    <div class="panel panel-{{$panel_type}}">
        <div class="panel-heading">Member Status</div>
        <div class="panel-body">
            <p>{{$full_name = $user->name_given . ' ' . $user->name_family }}
                {{'is '.$user->status}}
            </p>

            <form action="{{url('user/'.$update_status['type'].'/'.$user->id)}}" method="POST">
                {{ csrf_field()}}
                <button type="submit" id="update_status_{{$user->id}}" class="btn {{$update_status['btn_class']}} pull-right">
                    <i class="fa fa-btn-sm fa-{{$update_status['icon']}}"></i>{{$update_status['label']}}
                </button>
            </form>
        </div>
    </div>
@endif