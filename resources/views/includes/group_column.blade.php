<div class="col-md-3">
    @if(Auth::user()->status === 'active')
        @if(!$is_member)
            <div class="panel panel-default">
                <div class="panel-heading">Request Membership to {{$group->name}}</div>
                <div class="panel-body">
                    @if($is_pending)
                        <div class="panel-body">
                            Request Pending!
                        </div>
                    @else
                        <form action="{{ url('groups/'.$group->id.'/request') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-plus"></i> Join!
                                </button>
                            </div>
                        </form>
                    @endif
                </div>

            </div>
        @elseif(Auth::user()->id !== $group->created_by)
            <div class="panel panel-default">
                <div class="panel-heading">Withdraw from {{$group->name}}</div>

                <div class="panel-body">
                    <form action="{{ url('groups/'.$group->id.'/'. Auth::user()->id) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <div class="form-group">
                            <button type="submit" id="delete-task-{{ $group->id }}" class="btn btn-danger">
                                <i class="fa fa-minus"></i> Withdraw!
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        @endif
        @if(Auth::user()->id === $group->created_by || Auth::user()->roles === 'admin')
            <div class="panel panel-danger">
                <div class="panel-heading">NUKE {{$group->name}}</div>

                <div class="panel-body">
                    <form action="{{ url('/groups/'.$group->id) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <div class="form-group">
                            <button type="submit" id="delete-whole-{{ $group->id }}" class="btn btn-danger">
                                <i class="fa fa-trash"></i> DELETE!
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        @endif
    @endif
    @if($is_member || Auth::user()->roles ==='admin')
        <div class="panel panel-default">
            <div class="panel-heading">Members of {{$group->name}}</div>

            <div class="panel-body">
                @if(count($other_group_users)>0)
                    @foreach ($other_group_users as $user)
                        <div class="row">
                            <div class="col-md-10">
                                <a href="{{url('/users/' . $user->id)}}">{{$user->name_given}} {{$user->name_family}}</a>
                            </div>
                            <div class="col-md-2">
                                @if((Auth::user()->status === 'active') && (Auth::user()->roles === 'admin' || Auth::user()->id === $group->created_by))
                                    @if($user->id !== $group->created_by)
                                        <form action="{{ url('groups/'.$group->id.'/'. $user->id) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <div class="form-group pull-right">
                                                <button type="submit"
                                                        id="delete-whole-{{ $group->id }}"
                                                        class="btn btn-danger btn-xs">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            </div>
                                        </form>
                                    @endif
                                @endif
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">{{$group->name}} Events</div>
            <div class="panel-body">
                <a href="{{ url('/events/groups/' . $group->id) }}">Group Events</a>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">{{$group->name}} Interests</div>
            <div class="panel-body">
                @if(count($group_interests)>0)
                    <ul class="list-unstyled">
                        @foreach ($group_interests as $group_interest)
                            <li>{{$group_interest->name}}</li>
                        <!--a href="{{--url('/interest/'. $user_interest->id)--}}">{{--$user_interest->name--}}</a-->
                    @endforeach
                    <!--{{--@else // how sad, no interests whatsoever...--}}-->
                    </ul>
                @endif
            </div>
        </div>
    @endif
</div>
