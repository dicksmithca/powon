@if(isset($post) && ($content = $post->has_content()))
    <div>
        <label for="{{'photo'.$post->id}}">{{$content->label}}
            <img src="{{$content->path}}"
                 name="{{'photo'.$post->id}}"
                 style="height:200px; display: block;"
                 alt="{{$content->label}}"></label>
        {{--<video src="http://localhost/powon/resources/video/2016-08-06-211211.webm"></video>--}}
        @if(isset($edit) && $edit)
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="{{'delete_content_checkbox'.$post->id}}" value="delete">
                    Delete content
                </label>
            </div>
        @endif
    </div>
@endif

