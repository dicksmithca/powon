<div class="col-md-3">
    <div class="panel panel-default">
    <!--{{$is_same = ($user->id === Auth::user()->id)}}-->
    <!--{{$full_name = $user->name_given . ' ' . $user->name_family }}-->
        <div class="panel-heading">{{$full_name}}</div>

        <div class="panel-body">
            Email: {{$user->email}} <br>
            @if($is_same || Auth::user()->roles === 'admin'|| $user->details_permissions !== 'private')
                @if($is_same || Auth::user()->roles === 'admin'||
                    ($user->details_permissions === 'contacts' && $user->isContactOf(Auth::user())) ||
                    ($user->details_permissions === 'groups' && $user->isMemberOfSameGroup(Auth::user())))
            Date of Birth: {{$user->date_birthed}} <br>
            Address: {{$user->address}} <br>
            Gender: {{$user->gender}}<br>
            Date Joined: {{$user->created_at}} <br>
                @else
            User has hidden their information!
                @endif
            @else
            User has hidden their information!
            @endif
            @if($is_same && Auth::user()->status === 'active')
                <button type="button"
                        class="btn btn-primary"
                        data-toggle="modal"
                        data-target="#exampleModal"
                        data-whatever="@getbootstrap">
                    <i class="fa fa-btn fa fa-paper-plane"></i> Send Invitation
                </button>
                <div class="modal fade"
                     id="exampleModal"
                     tabindex="-1"
                     role="dialog"
                     aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" id="exampleModalLabel">Invitation</h4>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="form-group">
                                        <label for="recipient-name" class="form-control-label">Recipient:</label>
                                        <input type="text" class="form-control" id="recipient-name">
                                    </div>
                                    <div class="form-group">
                                        <label for="message-text" class="form-control-label">{{ $user->name_given }}
                                            ({{$user->email}}) [Birthday {{$user->date_birthed}}]:
                                        </label>
                                        <textarea class="form-control" id="message-text" rows="10">You’ve been cordially invited to the most awesome social network on the web, POWON. {{ $user->name_given }}
                                                                                                   , thinks you’re cool enough to join. We’re not sure that you are. It’s up to you to prove us wrong. Ball's in your court. Enter the credentials (top) and find out!
                                    </textarea>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button"
                                        class="btn btn-primary"
                                        onclick="alert('E-mail Sent!')"
                                        data-dismiss="modal">Send message
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
<!--{{$whos = ($is_same) ? "My" : $full_name.'\'s' }}-->
<!--{{$group_route = route('groups')}}-->

    <!--TODO visibility of elements should depend on type of contact, contact to self
    has full disclosure -->
    <div class="panel panel-default">
        <div class="panel-heading">{{$whos}} Groups</div>

        <div class="panel-body">
            @if($is_same || Auth::user()->roles === 'admin'|| $user->groups_permissions !== 'private')
                @if($is_same || Auth::user()->roles === 'admin'||
                    ($user->groups_permissions === 'contacts' && $user->isContactOf(Auth::user())) ||
                    ($user->groups_permissions === 'groups' && $user->isMemberOfSameGroup(Auth::user())))
                    @if(count($user_groups)>0)
                        <ul class="list-unstyled">
                        @foreach ($user_groups as $user_group)
                            <!--{{$listing_name = $user_group->name}}-->
                            @foreach($owned_groups as $owned_group)
                                @if($owned_group->id == $user_group->id)
                                    <!--{{$listing_name = $user_group->name.'*'}}-->
                                        @break
                                    @endif
                                @endforeach
                                <li><a href="{{url('/groups/' . $user_group->id)}}">{{$listing_name}}</a>

                                </li>
                            @endforeach
                        </ul>
                    @else
                        {!!($is_same) ? "I don't own any groups! Check out <a href=\"$group_route\">groups</a> to add!":"No groups!"!!}
                    @endif
                @else
                    User has hidden their information!
                @endif
            @else
                    User has hidden their information!
            @endif
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">{{$whos}} Contacts</div>

        <div class="panel-body">
            @if($is_same || Auth::user()->roles === 'admin'|| $user->contacts_permissions !== 'private')
                @if($is_same || Auth::user()->roles === 'admin'||
                    ($user->contacts_permissions === 'contacts' && $user->isContactOf(Auth::user())) ||
                    ($user->contacts_permissions === 'groups' && $user->isMemberOfSameGroup(Auth::user())))
                    @if(count($user_contacts)>0)
                        @foreach ($user_contacts as $user_contact)
                            <a href="{{url('/users/'. $user_contact->id)}}">
                                {{$user_contact->name_given}} {{$user_contact->name_family}}
                            </a>
                            @if($is_same)
                                <span class="pull-right">({{$user_contact->pivot->contact_id}})</span>
                            @endif
                            <br>
                        @endforeach
                    @else
                        {!!($is_same) ? "No contacts! Check out <a href=\"". url('users'). "\">users</a> to add!":"No contacts!"!!}
                    @endif
                @else
                    User has hidden their information!
                @endif
            @else
                    User has hidden their information!
            @endif
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">{{$whos}} Interests</div>
        <div class="panel-body">
            @if($is_same || Auth::user()->roles === 'admin'|| $user->attributes_permissions !== 'private')
                @if($is_same || Auth::user()->roles === 'admin'||
                    ($user->attributes_permissions === 'contacts' && $user->isContactOf(Auth::user())) ||
                    ($user->attributes_permissions === 'groups' && $user->isMemberOfSameGroup(Auth::user())))
                    @if(count($user_interests)>0)
                        <ul class="list-unstyled">
                            @foreach ($user_interests as $user_interest)
                                <li>{{$user_interest->name}}</li>
                            <!--a href="{{--url('/interest/'. $user_interest->id)--}}">{{--$user_interest->name--}}</a-->
                        @endforeach
                        <!--{{--@else // how sad, no interests whatsoever...--}}-->
                        </ul>
                    @endif
                @else
                    User has hidden their information!
                @endif
            @else
                    User has hidden their information!
            @endif
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">{{$whos}} Affiliations</div>
        <div class="panel-body">
            @if($is_same || Auth::user()->roles === 'admin'|| $user->attributes_permissions !== 'private')
                @if($is_same || Auth::user()->roles === 'admin'||
                    ($user->attributes_permissions === 'contacts' && $user->isContactOf(Auth::user())) ||
                    ($user->attributes_permissions === 'groups' && $user->isMemberOfSameGroup(Auth::user())))
                    @if(count($user_affiliations)>0)
                        <ul class="list-unstyled">
                            @foreach ($user_affiliations as $affiliation)
                                <li>{{$affiliation->name}}</li>
                        @endforeach
                        <!--{{--@else // how sad, no interests whatsoever...--}}-->
                        </ul>
                    @endif
                @else
                    User has hidden their information!
                @endif
            @else
                    User has hidden their information!
            @endif
        </div>
    </div>
    <!--
    Receive gifts on your profile
    Send gifts on another user's profile
    -->
    {{--@if(!$is_same)--}}
    {{--<div class="panel panel-default">--}}
    {{--<div class="panel-heading">Send Gift</div>--}}
    {{--<div class="panel-body">--}}
    {{--@if($user->gender === "female")--}}
    {{--<a> Show her you care </a> <br>--}}
    {{--@elseif($user->gender === "male")--}}
    {{--<a> Show him you care </a> <br>--}}
    {{--@else--}}
    {{--<a> Show them you care </a>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--@else--}}
    {{--<div class="panel panel-default">--}}
    {{--<div class="panel-heading"> Gifts Received </div>--}}
    {{--<div class="panel-body">--}}
    {{--@foreach ($gift_id as $gift)--}}
    {{--<li>{{$gift->gift_name}}</li>--}}
    {{--@endforeach--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--@endif--}}
</div>