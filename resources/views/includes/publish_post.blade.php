<?php
if ( $edit ) {
    $url          = '/edit/post/' . $group['id'] . '/' . $post->id;
    $text_input   = $post->text;
    $update_type  = 'post_edit';
    $button_text  = 'Save edits';
    $trigger_text = '<i class="fa fa-btn fa-pencil"></i>';
    $modal_title  = 'Edit post';
    $button_class = 'btn btn-warning btn-xs pull-right';
    $post_id      = $post->id;
} else {
    $update_type = 'post';
    $text_input  = '';
    if ( $top_post ) {
        $url          = '/post/' . $group['id']; // different route for top-level posting, $post is null
        $button_text  = 'Post to group';
        $trigger_text = 'Add a post';
        $modal_title  = 'Add post';
        $button_class = 'btn btn-primary btn-xs pull-right';
        $post_id      = 0;
    } else {
        $url          = '/post/' . $group['id'] . '/' . $post->id; // post_id here represents the parent post id
        $button_text  = 'Comment on post';
        $trigger_text = 'comment';
        $modal_title  = 'Comment on post';
        $button_class = 'btn btn-default btn-xs pull-right';
        $post_id      = $post->id;
    }
    $post = NULL; // avoid trying to get things from a non-existing post
}
?>


@if(Auth::user()->status === 'active')
    <!-- Trigger the modal with a button -->
    <button type="button"
            class="{{$button_class}}"
            data-toggle="modal"
            data-target={{'#'.$update_type.'Modal'.$post_id}}>{!!$trigger_text!!}</button>

    <!-- Modal -->
    <div id={{$update_type.'Modal'.$post_id}} class="modal" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{$modal_title}}</h4>
                </div>
                <div class="modal-body">
                    <div class="row form-inline">
                        <form action="{{url($url)}}"
                              method="POST"
                              enctype="multipart/form-data"
                              xmlns="http://www.w3.org/1999/html">
                            {{ csrf_field() }}

                            <div class="col-sm-12">
                                <div class="col-sm-7">
                                    <label for="{{$update_type.$post_id}}"></label>
                                    <textarea type="text"
                                              rows="10"
                                              cols="60"
                                              name="{{$update_type.$post_id}}"
                                              class="form-control">{{$text_input}}</textarea>
                                    *required</label>
                                    <div class="col-sm-11">
                                        <button type="submit" class="{{$button_class}}">{!!$button_text!!}</button>
                                    </div>
                                    @include('includes.content_show')
                                    @include('includes.upload')
                                </div>

                                <div class="col-sm-5"
                                     id="{{'permision_pane'.$post_id}}"
                                     style="max-height: 500px; overflow: auto">
                                    <div class="panel panel-default">
                                        <div class="panel panel-heading">{{(($edit) ? 'Modify' : 'Set').' user permissions:'}}</div>
                                        <div class="panel panel-body">
                                            <ul class="list-unstyled">
                                                @foreach($group->users()->diff(collect([Auth::user()])) as $group_user)
                                                    <?php
                                                    $can_comment = false;
                                                    $can_share = false;
                                                    ?>
                                                    @if($edit)
                                                        <?php
                                                        $can_comment = $post->can_comment($group_user->id);
                                                        $can_share = $post->can_share($group_user->id);
                                                        ?>
                                                    @endif
                                                    <li>
                                                        {{$group_user->name_given}} {{$group_user->name_family}}
                                                    </li>
                                                    <label for="r1">
                                                        <input type="radio"
                                                               name="{{'radio_permissions_'.$post_id.'_'.$group_user->id}}"
                                                               id="r1"
                                                               value="read only"
                                                               checked>
                                                        read
                                                    </label>
                                                    <label for="r2">
                                                        <input type="radio"
                                                               name="{{'radio_permissions_'.$post_id.'_'.$group_user->id}}"
                                                               id="r3"
                                                               value="comment"{{($can_comment) ? 'checked' : ''}}>
                                                        comment
                                                    </label>
                                                    <label for="r3">
                                                        <input type="radio"
                                                               name="{{'radio_permissions_'.$post_id.'_'.$group_user->id}}"
                                                               id="r4"
                                                               value="share" {{($can_share) ? 'checked' : ''}}>
                                                        share
                                                    </label>

                                                @endforeach
                                            </ul>
                                        </div>

                                        <!--/select></label-->

                                    </div>
                                </div>

                            </div>

                        </form>

                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>

        </div>
    </div>
@endif
