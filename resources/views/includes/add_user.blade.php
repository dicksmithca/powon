<div id="AddUserModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Contacts to Group</h4>
            </div>
            <div class="modal-body">
                <form action="{{url('usr/group/'.$group->id)}}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        @if(count($users_addable) > 0)
                            <label for="users_add">Select Users to Add!</label>
                            <select multiple="multiple" size="5" class="form-control" id="users_add" name="users_add[]">
                                @foreach($users_addable as $contact)
                                    <option value="{{$contact->id}}">{{$contact->name_given}} {{$contact->name_family}}</option>
                                @endforeach
                            </select>
                        @else
                            @if(Auth::user()->roles != 'admin')
                                <p>You have no contacts that are not part of this group</p>
                            @else
                                <p>All members belong to this group!<br>No one to add</p>
                            @endif
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-sm">
                            Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>