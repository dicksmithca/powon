@if(Auth::user()->status === 'active')
    <div class="panel panel-default">
    <?php
    $text = $post->text;
    $owner = $post->owner()
                  ->first();
    $full_name = $owner->name_given . ' ' . $owner->name_family;
    $date = $post->created_at;
    if ( !isset( $group ) ) { // we didn't come in via the group page but the user page
        $group = $post->groups()
                      ->get()
                      ->intersect(
                              Auth::user()
                                  ->groups()
                                  ->get())
                      ->first();
    }
    if ( $propagate ) { // propagate is used for groups view where user-view recent posts do not propagate for now
        $heading  = 'Posted by: ' . $full_name . ', on ' . $date . '-UTC';
        $children = $post->children()
                         ->get();
    } else {
        $heading = 'Posted by: ' . $full_name . ', on ' . $date . '-UTC on ' . $group['name'];
    }
    ?>
    <!--This modal is for commenting on a post TODO check permissions to show the button or not-->
        <div class="panel-heading">{{$heading}}
            @if($group->users()->contains(Auth::user()) && $post->can_comment(Auth::user()->id))
                @include('includes.publish_post', ['post' => $post,
                                                    'top_post' => false,
                                                    'edit' => false])
            @endif
        </div>
        <div class="panel-body">
            <div class="col-md-12">
                <!-- Post Delete Button -->

                <div class="btn-toolbar">
                    <!--TODO separate one button from the next and replace label for icon, connect functionality-->
                    <!--This opens up a modal to edit the post-->
                    @if(Auth::user()->id === $post->created_by) {{--TODO other permissions checks go here--}}
                    @include('includes.publish_post', ['post' => $post,
                                                    'top_post' => false,
                                                    'edit' => true])
                    @endif
                    @if(Auth::user()->roles === 'admin' || Auth::user()->id === $post->created_by)
                        <form action="{{url('post/'. $post->id)}}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" id="delete-post-{{ $post->id }}" class="btn btn-xs btn-danger pull-right">
                                <i class="fa fa-btn fa-trash"></i>
                            </button>

                        </form>
                    @endif
                </div>
            </div>

            <p>{{ $text }}</p>
            @include('includes.content_show')
            @if (  (isset($children)) && (count($children) > 0) )
                @foreach($children as $post)
                    @include('partials.posts', ['post' => $post,
                                                'propagate' => $propagate])
                @endforeach
            @endif
        </div>
    </div>
@endif