@extends('layouts.app')

@section('content')

    @if($is_member && Auth::user()->status === 'active')
        <div class="container">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div class="panel-heading"><h2>Event: {{$event->title}}</h2></div>

                    <div class="panel-body">

                        <p>{{$event->description}}</p>
                        <p>Event belongs to group: {{$group->name}}</p>
                        <p>Event created by: {{$owner->name_given}} {{$owner->name_family}}</p>
                        <ul>
                            <li>Starts at: {{$event->start}}</li>
                            <li>Ends at: {{$event->end}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Group user attendance-->
        <div class="container">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Group users attending:</h4></div>
                    <div class="panel-body">
                        @foreach ($respondedUsers as $response)
                            <ul>
                                <li>{{$response->name_given}} {{$response->name_family}} - {{$response->vote}}</li>
                            </ul>
                        @endforeach
                        <p>If a user name is not on the list, they have not yet submitted their response.</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Group user attendance-->
        @if(!$userRespondedAlready)
            <div class="container">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4>Will you be attending?</h4></div>

                        <div class="panel-body">

                            <form action="{{ url('events/'. $event->id) }}" method="POST">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label class="radio-inline">
                                        <input type="radio" id="yes" name="attending" value="yes">
                                        Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" id="no" name="attending" value="no">
                                        No
                                    </label>
                                    <button type="submit" class="btn btn-default">
                                        <i class="fa fa-plus"></i> Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    @else
        <h3>Sorry, but only active group members can see the group's events.</h3>
    @endif

@endsection