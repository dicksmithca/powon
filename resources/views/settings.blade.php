@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Settings</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/settings') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name_given') ? ' has-error' : '' }}">
                                <label for="name_given" class="col-md-4 control-label">First Name</label>

                                <div class="col-md-6">
                                    <input id="name_given"
                                           type="text"
                                           class="form-control"
                                           name="name_given"
                                           value="{{ $user->name_given }}">

                                    @if ($errors->has('name_given'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name_given') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('name_family') ? ' has-error' : '' }}">
                                <label for="name_family" class="col-md-4 control-label">Last Name</label>

                                <div class="col-md-6">
                                    <input id="name_family"
                                           type="text"
                                           class="form-control"
                                           name="name_family"
                                           value="{{ $user->name_family }}">

                                    @if ($errors->has('name_family'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name_family') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Email</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('date_birthed') ? ' has-error' : '' }}">
                                <label for="date_birthed" class="col-md-4 control-label">Date of Birth</label>

                                <div class="col-md-6">
                                    <input id="date_birthed"
                                           type="date"
                                           class="form-control"
                                           name="date_birthed"
                                           value="{{ $user->date_birthed }}">

                                    @if ($errors->has('date_birthed'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('date_birthed') }}</strong></span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                <label for="gender" class="col-md-4 control-label">Gender</label>

                                <div class="col-md-3">
                                    <input id="female"
                                           type="radio"
                                           class="form-control"
                                           name="gender"
                                           value="female" {{ $user->gender=="female" ? 'checked='.'"'.'checked'.'"' : '' }}>
                                    <label for="female"><i class="fa fa-female" aria-hidden="true"></i> Female</label>
                                </div>

                                <div class="col-md-3">
                                    <input id="male"
                                           type="radio"
                                           class="form-control"
                                           name="gender"
                                           value="male" {{ $user->gender=="male" ? 'checked='.'"'.'checked'.'"' : '' }}>
                                    <label for="male"><i class="fa fa-male" aria-hidden="true"></i> Male</label>
                                </div>

                                @if ($errors->has('gender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                @endif

                            </div>

                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="address" class="col-md-4 control-label">Address</label>

                                <div class="col-md-6">
                                    <input id="address"
                                           type="text"
                                           class="form-control"
                                           name="address"
                                           value="{{ $user->address }}">

                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('details_permissions') ? ' has-error' : '' }}">
                                <label for="details_permissions" class="col-md-4 control-label">Who can see your personal
                                                                                                details?
                                </label>

                                <div class="col-md-2">
                                    <input id="private"
                                           type="radio"
                                           class="form-control"
                                           name="details_permissions"
                                           value="private" {{ $user->details_permissions=="private" ? 'checked='.'"'.'checked'.'"' : '' }}>
                                    <label for="private"><i class="fa fa-user-secret" aria-hidden="true"></i> Private</label>
                                </div>

                                <div class="col-md-2">
                                    <input id="contacts"
                                           type="radio"
                                           class="form-control"
                                           name="details_permissions"
                                           value="contacts" {{ $user->details_permissions=="contacts" ? 'checked='.'"'.'checked'.'"' : '' }}>
                                    <label for="contacts"><i class="fa fa-user-plus" aria-hidden="true"></i> Contacts</label>
                                </div>

                                <div class="col-md-2">
                                    <input id="groups"
                                           type="radio"
                                           class="form-control"
                                           name="details_permissions"
                                           value="groups" {{ $user->details_permissions=="groups" ? 'checked='.'"'.'checked'.'"' : '' }}>
                                    <label for="groups"><i class="fa fa-users" aria-hidden="true"></i> Groups</label>
                                </div>

                                @if ($errors->has('details_permissions'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('details_permissions') }}</strong>
                                    </span>
                                @endif

                            </div>

                            <div class="form-group{{ $errors->has('contacts_permissions') ? ' has-error' : '' }}">
                                <label for="contacts_permissions" class="col-md-4 control-label">Who can see your list of
                                                                                                 contacts?
                                </label>

                                <div class="col-md-2">
                                    <input id="private"
                                           type="radio"
                                           class="form-control"
                                           name="contacts_permissions"
                                           value="private" {{ $user->contacts_permissions=="private" ? 'checked='.'"'.'checked'.'"' : '' }}>
                                    <label for="private"><i class="fa fa-user-secret" aria-hidden="true"></i> Private</label>
                                </div>

                                <div class="col-md-2">
                                    <input id="contacts"
                                           type="radio"
                                           class="form-control"
                                           name="contacts_permissions"
                                           value="contacts" {{ $user->contacts_permissions=="contacts" ? 'checked='.'"'.'checked'.'"' : '' }}>
                                    <label for="contacts"><i class="fa fa-user-plus" aria-hidden="true"></i> Contacts</label>
                                </div>

                                <div class="col-md-2">
                                    <input id="groups"
                                           type="radio"
                                           class="form-control"
                                           name="contacts_permissions"
                                           value="groups" {{ $user->contacts_permissions=="groups" ? 'checked='.'"'.'checked'.'"' : '' }}>
                                    <label for="groups"><i class="fa fa-users" aria-hidden="true"></i> Groups</label>
                                </div>

                                @if ($errors->has('contacts_permissions'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('contacts_permissions') }}</strong>
                                    </span>
                                @endif

                            </div>

                            <div class="form-group{{ $errors->has('groups_permissions') ? ' has-error' : '' }}">
                                <label for="groups_permissions" class="col-md-4 control-label">Who can see your list of groups?
                                </label>

                                <div class="col-md-2">
                                    <input id="private"
                                           type="radio"
                                           class="form-control"
                                           name="groups_permissions"
                                           value="private" {{ $user->groups_permissions=="private" ? 'checked='.'"'.'checked'.'"' : '' }}>
                                    <label for="private"><i class="fa fa-user-secret" aria-hidden="true"></i> Private</label>
                                </div>

                                <div class="col-md-2">
                                    <input id="contacts"
                                           type="radio"
                                           class="form-control"
                                           name="groups_permissions"
                                           value="contacts" {{ $user->groups_permissions=="contacts" ? 'checked='.'"'.'checked'.'"' : '' }}>
                                    <label for="contacts"><i class="fa fa-user-plus" aria-hidden="true"></i> Contacts</label>
                                </div>

                                <div class="col-md-2">
                                    <input id="groups"
                                           type="radio"
                                           class="form-control"
                                           name="groups_permissions"
                                           value="groups" {{ $user->groups_permissions=="groups" ? 'checked='.'"'.'checked'.'"' : '' }}>
                                    <label for="groups"><i class="fa fa-users" aria-hidden="true"></i> Groups</label>
                                </div>

                                @if ($errors->has('groups_permissions'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('groups_permissions') }}</strong>
                                    </span>
                                @endif

                            </div>

                            <div class="form-group{{ $errors->has('attributes_permissions') ? ' has-error' : '' }}">
                                <label for="attributes_permissions" class="col-md-4 control-label">Who can see your interests
                                                                                                   and affiliations?
                                </label>

                                <div class="col-md-2">
                                    <input id="private"
                                           type="radio"
                                           class="form-control"
                                           name="attributes_permissions"
                                           value="private" {{ $user->attributes_permissions=="private" ? 'checked='.'"'.'checked'.'"' : '' }}>
                                    <label for="private"><i class="fa fa-user-secret" aria-hidden="true"></i> Private</label>
                                </div>

                                <div class="col-md-2">
                                    <input id="contacts"
                                           type="radio"
                                           class="form-control"
                                           name="attributes_permissions"
                                           value="contacts" {{ $user->attributes_permissions=="contacts" ? 'checked='.'"'.'checked'.'"' : '' }}>
                                    <label for="contacts"><i class="fa fa-user-plus" aria-hidden="true"></i> Contacts</label>
                                </div>

                                <div class="col-md-2">
                                    <input id="groups"
                                           type="radio"
                                           class="form-control"
                                           name="attributes_permissions"
                                           value="groups" {{ $user->attributes_permissions=="groups" ? 'checked='.'"'.'checked'.'"' : '' }}>
                                    <label for="groups"><i class="fa fa-users" aria-hidden="true"></i> Groups</label>
                                </div>

                                @if ($errors->has('attributes_permissions'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('attributes_permissions') }}</strong>
                                    </span>
                                @endif

                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-floppy-o"></i> Save Settings
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Subscription Details</div>
                    <div class="panel-body">

                        <div class="form-group">
                            <label for="name_given" class="col-md-4 control-label">Card Brand</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" disabled value="{{ $user->card_brand }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name_given" class="col-md-4 control-label">Card Last 4</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" disabled value="{{ $user->card_last_four }}">
                            </div>
                        </div>

                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/payment') }}">
                            {{ csrf_field() }}
                            <div class="col-md-6 col-md-offset-4">
                                <script src="https://checkout.stripe.com/checkout.js"
                                        class="stripe-button"
                                        data-key="pk_test_rDWgdrAFg8VRFxxQqsDG8YDm"
                                        data-image="https://s3.amazonaws.com/stripe-uploads/acct_18UzosIGnxKmmQPumerchant-icon-1470679238763-powon.png"
                                        data-name="Yearly Subscription"
                                        data-panel-label="Update Card Details"
                                        data-label="Update Card Details"
                                        data-allow-remember-me=false
                                        data-locale="cad"></script>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
