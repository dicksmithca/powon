@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">POWON Member's interests</div>
                    <div class="panel-body">
                        @if (count($interests) > 0)
                            <ul class="list-unstyled">
                                @foreach ($interests as $interest)
                                    <li>
                                        {{ $interest->name }}
                                    </li>
                                @endforeach
                            </ul>
                        @else
                            <p>Actually, our members seem to be nihilists, they have interest in nothing</p>
                        @endif
                        @if(Auth::user()->status === 'active')
                            <form action="{{ url('interests/create') }}" method="POST">
                                <div class="form-group{{ $errors->has('interest_name') ? ' has-error' : '' }}">
                                    {{ csrf_field() }}
                                    <div class="col-sm-12">
                                        <div class="col-sm-9">
                                            <input type="text" name="interest_name" id="interest" class="form-control">
                                            @if ($errors->has('interest_name'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('interest_name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary pull-right">
                                            <i class="fa fa-btn fa-plus"></i>Add interest
                                        </button>
                                    </div>

                            </form>
                            @if (Session::has('message'))
                                <div>{{ Session::get('message') }}</div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection