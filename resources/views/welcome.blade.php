@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="jumbotron">
            <h2>Welcome to POWON!</h2>
            <h4>This is a webpage about things that things share with other things!</h4>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-6">
                    <h3>Announcements by your fav Admins!</h3>
                    @if (count($announcements) > 0)
                        @foreach ($announcements as $announcement)
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    {{ $announcement->title }}
                                </div>

                                <div class="panel-body">
                                    {{ $announcement->body }} <br> Posted on {{$announcement->updated_at}} UTC
                                    <!-- Task Delete Button -->
                                    @if(!Auth::guest())
                                        @if(Auth::user()->roles === 'admin')
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <form action="{{url('ann/' . $announcement->id)}}" method="POST">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                        <button type="submit"
                                                                id="delete-task-{{ $announcement->id }}"
                                                                class="btn btn-danger btn-sm">
                                                            <i class="fa fa-btn fa-trash"></i>Delete
                                                        </button>
                                                    </form>
                                                </div>
                                                <!-- Trigger the modal with a button -->
                                                <div class="col-md-3">
                                                    <button type="button"
                                                            class="btn btn-info btn-sm"
                                                            data-toggle="modal"
                                                            data-target="#myModal{{$announcement->id}}">Update
                                                    </button>
                                                </div>
                                            </div>
                                            <!-- Modal -->
                                            <div id="myModal{{$announcement->id}}" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button"
                                                                    class="close"
                                                                    data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Update Announcement</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="{{url('ann/'.$announcement->id)}}" method="POST">
                                                                {{ csrf_field() }}
                                                                <div class="form-group">
                                                                    <label for="announcement-title" class="control-label">New
                                                                        Announcement
                                                                        Title
                                                                    </label>
                                                                    <div>
                                                                        <input type="text"
                                                                               name="announcement-title"
                                                                               id="announcement-title"
                                                                               class="form-control" />
                                                                    </div>
                                                                </div>

                                                                <!-- Announcement Body -->
                                                                <div class="form-group">
                                                                    <label for="announcement-body" class="control-label">New
                                                                        Announcement
                                                                        body
                                                                    </label>
                                                                    <textarea name="announcement-body"
                                                                              id="announcement-body"
                                                                              class="form-control"></textarea>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="submit" class="btn btn-info btn-sm">
                                                                        Update
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @endif
                    @if(!Auth::guest())
                        @if(Auth::user()->roles === 'admin')
                            <form action="{{ url('ann') }}" method="POST" class="form-horizontal">
                            {{ csrf_field() }}

                            <!-- Announcement Name -->
                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                    <label for="title" class="control-label">Announcement Title</label>

                                    <div>
                                        <input type="text" name="title" id="title" class="form-control">
                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <!-- Announcement Body -->
                                <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                                    <label for="body" class="control-label">Announcement body</label>

                                    <div>
                                        <textarea name="body" id="body" class="form-control"></textarea>
                                        @if ($errors->has('body'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('body') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <!-- Add Task Button -->
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-6">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fa fa-plus"></i> Add Announcement
                                        </button>
                                    </div>
                                </div>
                            </form>
                        @endif
                    @endif
                </div>
                <div class="col-md-6">
                    <img height="400px"
                         width="400px"
                         src="http://images.clipartpanda.com/dr-seuss-coloring-pages-thing-1-and-thing-2-il_fullxfull.320861762.jpg">
                </div>
            </div>
        </div>
    </div>
@endsection
