@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if (count($users) > 0)
                    <div class="panel panel-default">
                        <div class="panel-heading">Filter user list</div>
                        <div class="panel-body">
                            <form action="{{url('users/search')}}" method="POST">
                                {{ csrf_field() }}
                                <div class="col-sm-12">
                                    <div class="col-sm-9">
                                        <p>By:</p>
                                        <label for="r1">
                                            <input type="radio" name="filter_field" id="r1" value="name" checked>
                                            Name
                                        </label>
                                        <label for="r2">
                                            <input type="radio" name="filter_field" id="r3" value="interests">
                                            Interests
                                        </label>
                                        <label for="r3">
                                            <input type="radio" name="filter_field" id="r4" value="affiliations">
                                            Affiliations
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" name="search_string" id="search" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-sm btn-primary pull-right">
                                        <i class="fa fa-btn fa-filter"></i>Filter
                                    </button>
                                </div>
                            </form>
                            @if (Session::has('message'))
                                <div>{{ Session::pull('message') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Members of POWON</div>
                        <div class="panel-body">
                            @if (Session::has('filtered_by'))
                                <div>{{ Session::pull('filtered_by') }}</div>
                                <form action="{{url('users/')}}" method="GET">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary btn-xs pull-right">
                                            <i class="fa fa-btn fa-clear"></i>Clear filter
                                        </button>
                                    </div>
                                </form>

                            @endif
                            <ul class="list-unstyled">
                                @foreach ($users as $user)
                                    @if (Auth::user()->id !== $user->id)
                                        <li><a href={{url('users/'.$user->id)}}>
                                                {{ $user->name_given }} {{ $user->name_family }}
                                            </a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
