@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!--Moved code out of here as it is re-usable by the (other) user_profile display user.blade.php-->
                @include('includes.profile_column')
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-heading">Recent Group Posts!</div>

                        <div class="panel-body">
                            <?php
                            $top_post = false;
                            ?>
                            @if (count($posts) > 0)
                                @foreach ($posts as $post)
                                    @include('partials.posts', ['post' => $post,
                                                                'propagate' => false])

                                @endforeach
                            @else
                                <p>There are no posts to display!</p>
                            @endif
                        </div>

                        <div class="panel-body"></div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Recent Conversations!</div>

                        <div class="panel-body">
                            @if(count($user_conversations) > 0)
                                @foreach($user_conversations as $user_convo)
                                    <a href="{{url('conversation/'.$user_convo->id)}}">{{$user_convo->title}}</a>
                                    Updated at {{$user_convo->updated_at}}
                                    <br>
                                @endforeach
                            @else
                                    No conversations! Go to <a href="{{route('conversations')}}"> conversations</a> to connect!
                            @endif
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Gifts Received!</div>
                        <div class="panel-body">
                            @if(count($user_gifts) > 0)
                                @foreach($user_gifts as $gift)

                                    @if($gift->gift_id === 1)
                                        {{$gift->name_given}} sent you a
                                        <a href="https://media.giphy.com/media/KAE8k5QsjXC8w/giphy.gif"> Whale!</a>

                                    @elseif($gift->gift_id === 2)
                                        {{$gift->name_given}} sent you a
                                        <a href="https://media.giphy.com/media/g8rhUpt0ounKM/giphy.gif"> Hug <3!</a>

                                    @elseif($gift->gift_id === 3)
                                        {{$gift->name_given}} sent you a
                                        <a href="https://media.giphy.com/media/13CoXDiaCcCoyk/giphy.gif"> Cat :3 Meow!</a>

                                    @elseif($gift->gift_id === 4)
                                        {{$gift->name_given}} sent you a
                                        <a href="http://31.media.tumblr.com/tumblr_m2ybagwZYO1qde8z7o1_500.gif"> DANCE
                                                                                                                 PARTY!</a>

                                    @endif
                                    <br>
                                @endforeach
                            @else
                                                              No Gifts!
                            @endif
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">New Requests!</div>

                        <div class="panel-body">
                            @if(count($user_requests)>0)
                                @foreach ($user_requests as $user_request)
                                    <div class="col-md-4">
                                        <a href="{{url('/users/'. $user_request->id)}}">
                                            {{$user_request->name_given}} {{$user_request->name_family}} <br> </a> <br>
                                    </div>
                                    <div class="col-md-6">
                                        <form action="{{ url('contact/'.$user_request->id) }}" method="POST">
                                            {{ csrf_field() }}
                                            @if(Auth::user()->status === 'active')
                                                <div class="form-group">
                                                    <label for="family">Family</label>
                                                    <input type="radio" name="relation" id="family" value="family" checked>
                                                    <label for="friend">Friend</label>
                                                    <input type="radio" name="relation" id="friend" value="friend">
                                                    <label for="colleague">Colleague</label>
                                                    <input type="radio" name="relation" id="colleague" value="colleague">
                                                    <button type="submit" class="btn btn-default">
                                                        <i class="fa fa-plus"></i> Accept {{$user_request->name_given}}!
                                                    </button>
                                                </div>
                                            @endif
                                        </form>
                                    </div>
                                @endforeach
                            @else
                                No requests!
                            @endif
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">New Group Requests!</div>

                        <div class="panel-body">
                            @foreach($group_requests as $group_request)
                                <a href="{{url('users/'.$group_request->user_id)}}">
                                    {{$group_request->name_given}} {{$group_request->name_family}}
                                </a> wants to join {{$group_request->name}}!
                                <form action="{{url('groups/'.$group_request->group_id .'/'. $group_request->user_id)}}"
                                      method="POST">
                                    {{ csrf_field()}}
                                    <button type="submit" id="" class="btn btn-success btn-xs">
                                        <i class="fa fa-btn fa-plus"></i>Accept!
                                    </button>
                                </form>
                                <br>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
