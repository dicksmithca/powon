@extends('layouts.app')
@if($user->status !== 'active')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-danger">
                    <div class="panel-heading">User status</div>
                    <div class="panel-body">
                        <p>Our records show this member is not currently active.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                @include('includes.change_user_status')
            </div>
        </div>
    </div>

@endsection
@else
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include('includes.profile_column')
                <div class="col-md-4">
                <!--{{$is_admin = (Auth::user()->roles === 'admin')}}-->
                    @if($is_admin && Auth::user()->id !== $user->id)
                        <div class="panel panel-default">
                            <div class="panel-heading">Admin Status</div>
                            <div class="panel-body">
                                <p>{{$full_name = $user->name_given . ' ' . $user->name_family }}
                                    {{$admin_status = $user->roles === 'admin' ? ' is an admin' : ' isn\'t an admin'}}
                                </p>
                                <?php
                                if ( $user->roles === 'admin' ) {
                                    $update_type  = "demote";
                                    $button_class = "btn-danger";
                                    $button_label = ' Demote';
                                    $button_icon  = 'level-down';
                                } else {
                                    $update_type  = "promote";
                                    $button_class = "btn-success";
                                    $button_label = 'Promote';
                                    $button_icon  = 'level-up';
                                }
                                ?>
                                <form action="{{url('user/'.$update_type.'/'.$user->id)}}" method="POST">
                                    {{ csrf_field()}}

                                    <button type="submit"
                                            id="update_user_{{$user->id}}"
                                            class="btn {{$button_class}} pull-right">
                                        <i class="fa fa-btn fa-{{$button_icon}}"></i>{{$button_label}}
                                    </button>
                                </form>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-md-4">
                    @include('includes.change_user_status')
                </div>
                <div class="col-md-8">
                    @if(Auth::user()->status === 'active')
                        <div class="panel panel-default">
                            <div class="panel-heading">Send Gifts</div>
                            <div class="panel-body">
                                <form action="{{url('/gifts/send/'.$user->id )}}" method="POST">
                                    <div class="row">
                                        {{ csrf_field() }}
                                        <div class="col-md-3">
                                            <img height="75px"
                                                 width="75px"
                                                 src="http://www.freedos.org/images/logos/fdfish-glossy-plain.svg">
                                            <input type="radio" name="gift_name" id="whale" value="1" checked>
                                        </div>
                                        <div class="col-md-3">
                                            <img height="75px"
                                                 width="75px"
                                                 src="https://s-media-cache-ak0.pinimg.com/564x/19/80/17/1980179191b50ac7e2e093ff36ee810d.jpg">
                                            <input type="radio" name="gift_name" id="hug" value="2" checked>
                                        </div>
                                        <div class="col-md-3">
                                            <img width="75px"
                                                 height="75px"
                                                 src="http://cliparts.co/cliparts/8cG/6aX/8cG6aXani.png">
                                            <input type="radio" name="gift_name" id="cat" value="3" checked>
                                        </div>
                                        <div class="col-md-3">
                                            <img width="75px"
                                                 height="75px"
                                                 src="http://hooplaha.com/wp-content/uploads/2013/01/single-ladies-shuffle.gif">
                                            <input type="radio" name="gift_name" id="dance" value="4" checked>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <div class="row">
                                            <button type="submit" id="send_id" class="btn btn-primary">
                                                <i class="fa fa-btn fa-gift"></i>Send Gift
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <div class="col-md-12 text-center">
                                    <div class="row">
                                        <br>
                                    </div>
                                </div>
                                @if (Session::has('message'))
                                    <div class="col-md-12 text-center">
                                        <div class="alert alert-success text-center">
                                            <strong>{{ Session::get('message') }}</strong>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    @endif
                    @if(!$is_contact)
                        @if(count($contact_request) === 0 )
                            <div class="col-md-4">
                                @if(Auth::user()->status === 'active')
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Request to be friend!</div>
                                        <div class="panel-body">
                                            <form action="{{ url('contact/'.$user->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label for="family">Family</label>
                                                    <input type="radio" name="relation" id="family" value="family" checked>
                                                    <label for="friend">Friend</label>
                                                    <input type="radio" name="relation" id="friend" value="friend">
                                                    <label for="colleague">Colleague</label>
                                                    <input type="radio" name="relation" id="colleague" value="colleague">
                                                    <button type="submit" class="btn btn-default">
                                                        <i class="fa fa-plus"></i> Add {{$user->name_given}}!
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
@endsection
@endif