@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Conversation: <strong>{{$convo_owner->title}}</strong><br> Contacts:
                    @foreach($users_in_convo as $u_c)
                        @if(Auth::user()->id !== $u_c->id)
                            {{$u_c->name_given}},
                        @endif
                    @endforeach
                    @if(Auth::user()->status === 'active' && (Auth::user()->id === $convo_owner->created_by || Auth::user()->roles === 'admin'))
                        <div class="pull-right">
                            <form action="{{url('conversation/' . $conv_id)}}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" id="delete-task-{{ $conv_id }}" class="btn btn-danger btn-sm">
                                    <i class="fa fa-btn fa-trash"></i>
                                </button>
                            </form>
                        </div>
                    @endif
                </div>

                <div class="panel-body">
                    @foreach($messages as $message)
                        @if(Auth::user()->id !== $message->id)
                            <div class="row"></div>
                            <div class="well well-sm col-md-6 pull-left">
                                {{$message->name_given}} {{$message->name_family}} said: {{$message->body}}
                            </div>
                        @else
                            <div class="row"></div>
                            <div class="well well-sm col-md-6 pull-right" style="text-align: right">
                                Me: {{$message->body}}
                            </div>
                        @endif
                    @endforeach

                </div>
            </div>
            @if(Auth::user()->status === 'active')
                <form action="{{ url('conversation/'.$conv_id. '/new_message') }}" method="POST" class="form-horizontal">
                {{ csrf_field() }}

                <!-- Message Body -->
                    <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                        <label for="body" class="control-label">Enter Message:</label>
                        <div>
                            <textarea name="body" id="body" class="form-control"></textarea>
                            @if ($errors->has('body'))
                                <span class="help-block">
                                            <strong>{{ $errors->first('body') }}</strong>
                                            </span>
                            @endif
                        </div>
                    </div>

                    <!-- Add Message -->
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6">
                            <button type="submit" class="btn btn-default">
                                <i class="fa fa-plus"></i> Add Message!
                            </button>
                        </div>
                    </div>
                </form>
            @endif
        </div>
    </div>
@endsection