@extends('layouts.app')

@section('content')
    <!-- formatting-->
    <div class="container">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                @if($is_member && Auth::user()->status === 'active')
                    <div class="panel-heading">Create New Event!</div>
                    <div class="panel-body">
                        <div class="col-md-8 col-md-offset-2">
                            <form action="{{ url('events/groups/' . $group_id) }}" method="POST" class="form-horizontal">
                            {{ csrf_field() }}

                            <!-- Event Name -->
                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                    <label for="name" class="control-label">Event Title</label>

                                    <div>
                                        <input type="text" name="title" id="event-title" class="form-control">
                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <!-- Event Description -->
                                <div class="form-group
                                <div class=" form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="description" class="control-label">Event Description</label>
                                <div>
                                    <textarea type="text"
                                              name="description"
                                              id="event-description"
                                              class="form-control"></textarea>
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                        </div>
                        <!-- Start-->
                        <div class="form-group{{ $errors->has('start') ? ' has-error' : '' }}">
                            <label for="start" class="control-label">Start</label>

                            <div>
                                <input type="datetime-local"
                                       name="start"
                                       id="start"
                                       class="form-control"
                                       placeholder="YYYY-MM-DD HH:MI:SE">
                                @if ($errors->has('start'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('start') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!--- End date-->
                        <div class="form-group{{ $errors->has('end') ? ' has-error' : '' }}">
                            <label for="end" class="control-label">End</label>
                            <div>
                                <input type="datetime-local"
                                       name="end"
                                       id="end"
                                       class="form-control"
                                       placeholder="YYYY-MM-DD HH:MI:SE">
                                @if ($errors->has('end'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('end') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- Add Task Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-plus"></i> Create Event
                                </button>
                            </div>
                        </div>
                        </form>
                    </div>
            </div>
        </div>
        <h2>Events for group {{$group_name}}</h2>
        <div class="panel-heading"><h3>List of events</h3></div>
        <div class="panel-body">
            @if(count($events)>0)

                <ul>
                    @foreach ($events as $event)

                        <li><a href="{{ url('/events/' . $event->id) }}">{{ $event->title }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            @else
                <h4>This group doesn't have any events yet. Please create one!</h4>
            @endif
        </div>
    </div>
    </div>
    @else
        <h3>Sorry, but only active group members can see the group's events.</h3>
    @endif
@endsection