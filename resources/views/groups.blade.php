@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="col-md-8 col-md-offset-2">
            @if(Auth::user()->status === 'active')
                <div class="panel panel-default">
                    <div class="panel-heading">Create New Group!</div>
                    <div class="panel-body">
                        <div class="col-md-8 col-md-offset-2">
                            <form action="{{ url('group') }}" method="POST" class="form-horizontal">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="control-label">Group Name</label>

                                    <div>
                                        <input type="text" name="name" id="group-name" class="form-control">
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                    <label for="description" class="control-label">Group Description</label>

                                    <div>
                                        <textarea name="description" id="group-description" class="form-control"></textarea>
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <!-- Add Group Button -->
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-6">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fa fa-plus"></i> Add Group
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endif
            @if (count($groups) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">POWON's Group List</div>
                    <div class="panel-body">
                        @foreach ($groups as $group)
                            <a href="{{ url('/groups/' . $group->id) }}">{{ $group->name }}</a>
                            created by {{$group->owner->name_given}} {{$group->owner->name_family}} on {{$group->created_at}}
                            <br>

                        @endforeach
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection