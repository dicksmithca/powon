@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if(Auth::user()->status === 'active')
                    <div class="panel panel-default">
                        <div class="panel-heading">Create New Conversation</div>
                        <div class="panel-body">
                            <div class="col-md-6 col-md-offset-3">
                                @if(count($user_contacts) > 0)
                                    <form action="{{ route('create_convo') }}" method="POST" class="form-horizontal">
                                        {{ csrf_field() }}
                                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                            <label for="title" class="control-label">Conversation Title*</label>
                                            <div>
                                                <input type="text" name="title" id="conversation-title" class="form-control">
                                                @if ($errors->has('title'))
                                                    <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('users') ? ' has-error' : '' }}">
                                            <label for="users" class="control-label">Start a conversation with*</label>
                                            <div>
                                                <select multiple="multiple"
                                                        size="7"
                                                        class="form-control"
                                                        id="sel1"
                                                        name="users[]">
                                                    @foreach($user_contacts as $user_contact)
                                                        <option value="{{$user_contact->id}}">{{$user_contact->name_given}} {{$user_contact->name_family}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('users'))
                                                    <span class="help-block">
                                            <strong>{{ $errors->first('users') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-default">
                                                <i class="fa fa-plus"></i> Start New Conversation!
                                            </button>
                                        </div>
                                    </form>
                                @else
                                    You have no contacts! Make some <a href="{{url('users')}}">contacts</a>!
                                @endif
                            </div>
                        </div>
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Conversations</div>
                    <div class="panel-body">
                        @if(count($user_convos) > 0)
                            {{--{{$user_convos}}--}}
                            @foreach($user_convos as $key => $user_convo)
                                <a href="{{url('conversation/'. $key)}}"> {{$user_convo[0]->title}} with:
                                    @foreach ($user_convo as $key2 => $u_c)
                                        @if($u_c->user_id !== Auth::user()->id)
                                            {{ $u_c->name_given }} {{$u_c->name_family}},
                                        @endif
                                    @endforeach
                                </a> Started on {{$u_c->created_at}} <br>
                            @endforeach
                        @else
                                     No conversations! Create one above!
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection