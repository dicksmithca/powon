# POWON - COMP 5531 - Group 1
* Marina Chirchikova 27581971
* Robert Maglieri 40013562
* Federico Regueiro 40012304
* D. Richard Smith 27859279
* Eric Tang 40009504
## Deploy Locally
##### Install Composer (https://getcomposer.org/download/)
1. php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
2. php composer-setup.php --install-dir=bin --filename=composer
3. php -r "unlink('composer-setup.php');"
##### Setup DB
1. Create schema hoc55311
2. Create user hoc55311:TextToSp
##### Deploy
1. git clone https://bitbucket.org/dicksmithca/powon.git
2. cd powon/
3. cp .env.example .env
4. cd .. && chmod -R 777 powon/
5. cd powon/ && php artisan migrate
6. cd .. && chmod -R 777 powon/
7. cd powon/ && php artisan db:seed
8. php artisan serve
9. http://localhost:8000/
## Deploying to ENCS
##### Install Composer (https://getcomposer.org/download/)
1. php -d allow_url_fopen=1 -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
2. php -d allow_url_fopen=1 composer-setup.php --install-dir=bin --filename=composer
3. php -r "unlink('composer-setup.php');"
##### Setup DB
1. Drop all tables from hoc55311
##### Deploy
1. ssh login.encs.concordia.ca
2. cd /www/groups/h/ho_comp5531_1/
3. rm -rf powon/
4. mv powon ARCH
5. git clone https://dicksmith_bitbucket@bitbucket.org/dicksmithca/powon.git
6. cd powon/ && php -d allow_url_fopen=1 ~/bin/composer install
7. cd .. && chmod -R 777 powon/
8. cd powon/ && php artisan migrate
9. cd .. && chmod -R 777 powon/
10. cd powon/ && php artisan db:seed
## Testing
1. App is deployed to [https://hoc55311.encs.concordia.ca]
    * ID: hoc55311
    * PW: TextToSp
2. On initial install the only user created is
    * ID: admin@powon
    * PW: admin
3. And must have password changed after login.
4. Registering new users requires a test credit card, with a current expiration date, and CVV numbe rof correct length. For example:
    * 4111 1111 1111 1111
    * 11/21
    * 111
    Other cards can be found at [https://stripe.com/docs/testing].
