<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create(
            'events',
            function (Blueprint $table) {

                $table->increments('id');
                $table->integer('group_id')
                      ->unsigned();
                $table->integer('user_id')
                      ->unsigned();
                $table->string('title', 63);
                $table->string('description', 1023)
                      ->nullable();
                $table->dateTime('start');
                $table->dateTime('end');
                $table->timestamps();
                $table->foreign('group_id')
                      ->references('id')
                      ->on('groups')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
                $table->foreign('user_id')
                      ->references('id')
                      ->on('users')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('events');
    }
}
