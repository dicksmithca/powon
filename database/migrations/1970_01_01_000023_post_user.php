<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PostUser extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create(
            'post_user',
            function (Blueprint $table) {

                $table->integer('post_id')
                      ->unsigned();
                $table->integer('user_id')
                      ->unsigned();
                $table->enum(
                    'permission',
                    [ 'read',
                      'comment',
                      'share' ])
                      ->default('read');
                $table->primary(
                    [ 'post_id',
                      'user_id' ]);
                $table->timestamps();
                $table->foreign('post_id')
                      ->references('id')
                      ->on('posts')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
                $table->foreign('user_id')
                      ->references('id')
                      ->on('users')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
            });//
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('post_user');
    }
}
