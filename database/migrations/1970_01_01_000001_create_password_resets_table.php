<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePasswordResetsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create(
            'password_resets',
            function (Blueprint $table) {

                $table->string('email')
                      ->index();
                $table->string('token')
                      ->index();
                $table->primary(
                    [ 'email',
                      'token' ]);
                $table->timestamp('created_at')
                      ->nullable();
                $table->foreign('email')
                      ->references('email')
                      ->on('users')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('password_resets');
    }
}
