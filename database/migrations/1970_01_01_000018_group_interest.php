<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class GroupInterest extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create(
            'group_interest',
            function (Blueprint $table) {

                $table->integer('group_id')
                      ->unsigned();
                $table->integer('interest_id')
                      ->unsigned();
                $table->primary(
                    [ 'group_id',
                      'interest_id' ]);
                $table->timestamps();
                $table->foreign('group_id')
                      ->references('id')
                      ->on('groups')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
                $table->foreign('interest_id')
                      ->references('id')
                      ->on('interests')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('group_interest');
    }
}
