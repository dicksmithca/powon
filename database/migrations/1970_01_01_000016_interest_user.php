<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class InterestUser extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create(
            'interest_user',
            function (Blueprint $table) {

                $table->integer('interest_id')
                      ->unsigned();
                $table->integer('user_id')
                      ->unsigned();
                $table->primary(
                    [ 'interest_id',
                      'user_id' ]);
                $table->timestamps();
                $table->foreign('interest_id')
                      ->references('id')
                      ->on('interests')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
                $table->foreign('user_id')
                      ->references('id')
                      ->on('users')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('interest_user');
    }
}
