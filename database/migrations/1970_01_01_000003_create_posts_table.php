<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create(
            'posts',
            function (Blueprint $table) {

                $table->increments('id');
                $table->integer('parent_id')
                      ->unsigned()
                      ->nullable();
                $table->integer('created_by')
                      ->unsigned();
                $table->text('text');
                $table->timestamps();
                $table->date('date_expired')
                      ->nullable();
                $table->foreign('created_by')
                      ->references('id')
                      ->on('users')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');

            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('posts');
    }
}
