<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create(
            'users',
            function (Blueprint $table) {

                $table->increments('id');
                $table->string('name_family', 63);
                $table->string('name_given', 63);
                $table->date('date_birthed');
                $table->enum(
                    'status',
                    [ 'active',
                      'inactive',
                      'suspended', ])
                      ->default('active');
                $table->enum(
                    'gender',
                    [ 'female',
                      'male', ]);
                $table->string('address');
                $table->string('email')
                      ->unique();
                $table->string('password');
                $table->boolean('password_invalid')
                      ->default(false);
                $table->enum(
                    'details_permissions',
                    [ 'private',
                      'contacts',
                      'groups', ])
                      ->default('private');
                $table->enum(
                    'contacts_permissions',
                    [ 'private',
                      'contacts',
                      'groups', ])
                      ->default('private');
                $table->enum(
                    'groups_permissions',
                    [ 'private',
                      'contacts',
                      'groups', ])
                      ->default('private');
                $table->enum(
                    'attributes_permissions',
                    [ 'private',
                      'contacts',
                      'groups', ])
                      ->default('private');
                $table->enum(
                    'roles',
                    [ 'member',
                      'admin', ])
                      ->default('member');
                $table->rememberToken();
                $table->timestamps();
            });

        // Create Default Admin
        DB::table('users')
          ->insert(
              [ 'name_family'      => 'POWON',
                'name_given'       => 'Admin',
                'email'            => 'admin@powon',
                'password'         => bcrypt('admin'),
                'password_invalid' => true,
                'date_birthed'     => Carbon::now(),
                'status'           => 'active',
                'gender'           => 'female',
                'address'          => 'Admin',
                'roles'            => 'admin',
                'created_at'       => Carbon::now(), ]);

        // For cashier
        Schema::table(
            'users',
            function (Blueprint $table) {

                $table->string('stripe_id')
                      ->nullable();
                $table->string('card_brand')
                      ->nullable();
                $table->string('card_last_four')
                      ->nullable();
                $table->timestamp('trial_ends_at')
                      ->nullable();
            });

        // For cashier
        Schema::create(
            'subscriptions',
            function (Blueprint $table) {

                $table->increments('id');
                $table->integer('user_id')
                      ->unsigned();
                $table->string('name');
                $table->string('stripe_id');
                $table->string('stripe_plan');
                $table->integer('quantity');
                $table->timestamp('trial_ends_at')
                      ->nullable();
                $table->timestamp('ends_at')
                      ->nullable();
                $table->timestamps();
                $table->foreign('user_id')
                      ->references('id')
                      ->on('users')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        // For cashier
        Schema::drop('subscriptions');

        Schema::drop('users');
    }
}
