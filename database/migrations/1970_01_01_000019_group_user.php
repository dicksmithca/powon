<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class GroupUser extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create(
            'group_user',
            function (Blueprint $table) {

                $table->integer('group_id')
                      ->unsigned();
                $table->integer('user_id')
                      ->unsigned();
                $table->timestamp('approved_at')
                      ->nullable();
                $table->primary(
                    [ 'group_id',
                      'user_id' ]);
                $table->timestamps();
                $table->foreign('group_id')
                      ->references('id')
                      ->on('groups')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
                $table->foreign('user_id')
                      ->references('id')
                      ->on('users')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('group_user');
    }
}
