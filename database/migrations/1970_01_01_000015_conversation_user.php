<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ConversationUser extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create(
            'conversation_user',
            function (Blueprint $table) {

                $table->integer('conversation_id')
                      ->unsigned();
                $table->integer('user_id')
                      ->unsigned();
                $table->primary(
                    [ 'conversation_id',
                      'user_id' ]);
                $table->timestamps();
                $table->foreign('conversation_id')
                      ->references('id')
                      ->on('conversations')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
                $table->foreign('user_id')
                      ->references('id')
                      ->on('users')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('conversation_user');
    }
}
