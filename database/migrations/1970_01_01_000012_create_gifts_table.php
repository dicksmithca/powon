<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGiftsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create(
            'gifts',
            function (Blueprint $table) {

                $table->increments('id')
                      ->unsigned();
                $table->string('gift_name');
            });

        DB::table('gifts')
          ->insert(
              [ 'id'        => '1',
                'gift_name' => 'whale', ]);

        DB::table('gifts')
          ->insert(
              [ 'id'        => '2',
                'gift_name' => 'hug', ]);

        DB::table('gifts')
          ->insert(
              [ 'id'        => '3',
                'gift_name' => 'cat', ]);

        DB::table('gifts')
          ->insert(
              [ 'id'        => '4',
                'gift_name' => 'dance', ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('gifts');
    }
}
