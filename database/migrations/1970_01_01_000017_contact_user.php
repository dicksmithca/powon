<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ContactUser extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create(
            'contact_user',
            function (Blueprint $table) {

                $table->integer('source_user_id')
                      ->unsigned();
                $table->integer('target_user_id')
                      ->unsigned();
                $table->integer('contact_id')
                      ->unsigned();
                $table->primary(
                    [ 'source_user_id',
                      'target_user_id' ]);
                $table->timestamps();
                $table->foreign('source_user_id')
                      ->references('id')
                      ->on('users')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
                $table->foreign('target_user_id')
                      ->references('id')
                      ->on('users')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
                $table->foreign('contact_id')
                      ->references('id')
                      ->on('contacts')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('contact_user');
    }
}
