<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create(
            'contacts',
            function (Blueprint $table) {

                $table->increments('id');
                $table->string('name')
                      ->unique();
            });

        // Add the convened relation types
        DB::table('contacts')
          ->insert(
              [ 'name' => 'family', ]);

        DB::table('contacts')
          ->insert(
              [ 'name' => 'friends', ]);

        DB::table('contacts')
          ->insert(
              [ 'name' => 'colleagues', ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('contacts');
    }
}
