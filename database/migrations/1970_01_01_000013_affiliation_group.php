<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AffiliationGroup extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create(
            'affiliation_group',
            function (Blueprint $table) {

                $table->integer('affiliation_id')
                      ->unsigned();
                $table->integer('group_id')
                      ->unsigned();
                $table->primary(
                    [ 'affiliation_id',
                      'group_id' ]);
                $table->timestamps();
                $table->foreign('affiliation_id')
                      ->references('id')
                      ->on('affiliations')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
                $table->foreign('group_id')
                      ->references('id')
                      ->on('groups')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('affiliation_group');
    }
}
