<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class GroupPost extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create(
            'group_post',
            function (Blueprint $table) {

                $table->integer('group_id')
                      ->unsigned();
                $table->integer('post_id')
                      ->unsigned();
                $table->primary(
                    [ 'group_id',
                      'post_id' ]);
                $table->timestamps();
                $table->foreign('group_id')
                      ->references('id')
                      ->on('groups')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
                $table->foreign('post_id')
                      ->references('id')
                      ->on('posts')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('group_post');
    }
}