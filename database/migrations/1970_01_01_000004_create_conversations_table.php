<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConversationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create(
            'conversations',
            function (Blueprint $table) {

                $table->increments('id');
                $table->string('title');
                $table->integer('created_by')
                      ->unsigned();
                $table->integer('updated_by')
                      ->unsigned();
                $table->timestamps();
                $table->foreign('created_by')
                      ->references('id')
                      ->on('users')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
                $table->foreign('updated_by')
                      ->references('id')
                      ->on('users')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('conversations');
    }
}
