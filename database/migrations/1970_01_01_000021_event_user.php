<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class EventUser extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create(
            'event_user',
            function (Blueprint $table) {

                $table->integer('event_id')
                      ->unsigned();
                $table->integer('user_id')
                      ->unsigned();
                $table->enum(
                    'vote',
                    [ 'no',
                      'yes' ]);
                $table->primary(
                    [ 'event_id',
                      'user_id' ]);
                $table->timestamps();
                $table->foreign('event_id')
                      ->references('id')
                      ->on('events')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
                $table->foreign('user_id')
                      ->references('id')
                      ->on('users')
                      ->onDelete('restrict')
                      ->onUpdate('cascade');
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('event_user');
    }
}