<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGroupsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create(
            'groups',
            function (Blueprint $table) {

                $table->increments('id');
                $table->string('name', 63)
                      ->unique();
                $table->string('description', 511)
                      ->nullable();
                $table->integer('created_by')
                      ->unsigned();
                $table->timestamps();
                $table->foreign('created_by')
                      ->references('id')
                      ->on('users')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('groups');
    }
}
