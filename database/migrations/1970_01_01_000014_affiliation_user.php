<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AffiliationUser extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create(
            'affiliation_user',
            function (Blueprint $table) {

                $table->integer('affiliation_id')
                      ->unsigned();
                $table->integer('user_id')
                      ->unsigned();
                $table->primary(
                    [ 'affiliation_id',
                      'user_id' ]);
                $table->timestamps();
                $table->foreign('affiliation_id')
                      ->references('id')
                      ->on('affiliations')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
                $table->foreign('user_id')
                      ->references('id')
                      ->on('users')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('affiliation_user');
    }
}
