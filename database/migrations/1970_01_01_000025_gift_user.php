<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class GiftUser extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create(
            'gift_user',
            function (Blueprint $table) {

                $table->integer('gift_id')
                      ->unsigned();
                $table->integer('receiver_id')
                      ->unsigned();
                $table->integer('sender_id')
                      ->unsigned();
                $table->timestamps();
                $table->primary(
                    [ 'gift_id',
                      'receiver_id',
                      'sender_id', ]);
                $table->foreign('gift_id')
                      ->references('id')
                      ->on('gifts')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
                $table->foreign('receiver_id')
                      ->references('id')
                      ->on('users')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');

                $table->foreign('sender_id')
                      ->references('id')
                      ->on('users')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('gift_user');
    }
}
