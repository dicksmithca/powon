<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        // $this->call(UsersTableSeeder::class);

        DB::table('users')
          ->insert(
              [ 'name_given'     => 'noone',
                'name_family'    => 'who',
                'email'          => 'noone.who@gmail.com',
                'password'       => bcrypt('password'),
                'date_birthed'   => Carbon::now(),
                'gender'         => 'male',
                'remember_token' => str_random(10),
                'roles'          => 'admin' ]);

        DB::table('users')
          ->insert(
              [ 'name_given'     => 'Loser',
                'name_family'    => 'McLoserson',
                'email'          => 'loser@gmail.com',
                'password'       => bcrypt('password'),
                'date_birthed'   => Carbon::now(),
                'gender'         => 'male',
                'remember_token' => str_random(10),
                'roles'          => 'member',
                'status'         => 'inactive' ]);

        DB::table('users')
          ->insert(
              [ 'name_given'     => 'Melvin',
                'name_family'    => 'Kelvin',
                'email'          => 'winless@gmail.com',
                'password'       => bcrypt('password'),
                'date_birthed'   => Carbon::now(),
                'gender'         => 'male',
                'remember_token' => str_random(10),
                'roles'          => 'member',
                'status'         => 'suspended' ]);

        DB::table('users')
          ->insert(
              [ 'name_given'     => 'Norman',
                'name_family'    => 'Normie',
                'email'          => 'normie@gmail.com',
                'password'       => bcrypt('password'),
                'date_birthed'   => Carbon::now(),
                'gender'         => 'male',
                'remember_token' => str_random(10),
                'roles'          => 'member',
                'status'         => 'active' ]);
        factory(App\User::class, 50)->create();

        $user_count = DB::table('users')
                        ->count();

        for ( $i = 0; $i < $user_count * 4; $i++ ) {
            $source_id = mt_rand(1, $user_count);
            do {
                $target_id = mt_rand(1, $user_count);
            } while ( $target_id === $source_id );
            if ( DB::table('contact_user')
                   ->where('source_user_id', $source_id)
                   ->where('target_user_id', $target_id)
                   ->count() === 0
            ) {
                DB::table('contact_user')
                  ->updateOrInsert(
                      [ 'source_user_id' => $source_id,
                        'target_user_id' => $target_id,
                        'contact_id'     => 1 ]);
            }
        }

        DB::table('announcements')
          ->insert(
              [ 'title'      => 'Welcome',
                'body'       => 'POWON, the thing to share things',
                'created_by' => 2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now() ]);

        DB::table('interests')
          ->insert(
              [ 'name' => 'oatmeal cookies' ]);

        DB::table('interests')
          ->insert(
              [ 'name' => 'fishing' ]);

        DB::table('interests')
          ->insert(
              [ 'name' => 'oatmeal cookies with herring' ]);

        DB::table('groups')
          ->insert(
              [ 'name'        => 'Bakery',
                'description' => 'Love them doughy goodies',
                'created_by'  => 1,
                'created_at'  => Carbon::now() ]);

        DB::table('group_user')
          ->insert(
              [ 'group_id'    => 1,
                'user_id'     => 1,
                'approved_at' => Carbon::now() ]);

        DB::table('groups')
          ->insert(
              [ 'name'        => 'Fishing',
                'description' => 'Me likes fishessss',
                'created_by'  => 1,
                'created_at'  => Carbon::now() ]);

        DB::table('group_user')
          ->insert(
              [ 'group_id'    => 2,
                'user_id'     => 1,
                'approved_at' => Carbon::now() ]);

        DB::table('groups')
          ->insert(
              [ 'name'        => 'Hammock coding intl.',
                'description' => 'If only the neighbour\'s AC didn\'t make so much damn noise',
                'created_by'  => 2,
                'created_at'  => Carbon::now() ]);

        DB::table('groups')
          ->insert(
              [ 'name'        => 'None shall pass',
                'description' => 'This group is too exclusive for you',
                'created_by'  => 1,
                'created_at'  => Carbon::now() ]);

        DB::table('group_user')
          ->insert(
              [ 'group_id'    => 3,
                'user_id'     => 2,
                'approved_at' => Carbon::now() ]);

        DB::table('affiliations')
          ->insert(
              [ 'name'       => 'Concordia University',
                'created_at' => Carbon::now() ]);

        for ( $i = 3; $i <= $user_count; $i++ ) {
            DB::table('interest_user')
              ->insert(
                  [ 'interest_id' => random_int(1, 3),
                    'user_id'     => $i ]);
            // make some group members approved, some not
            if ( mt_rand(0, 1) || ( $i === 1 ) ) {
                DB::table('group_user')
                  ->insert(
                      [ 'group_id'    => random_int(1, 2),
                        'user_id'     => $i,
                        'approved_at' => Carbon::now() ]);
            } else {
                DB::table('group_user')
                  ->insert(
                      [ 'group_id' => random_int(1, 2),
                        'user_id'  => $i ]);
            }
            DB::table('affiliation_user')
              ->insert(
                  [ 'affiliation_id' => 1,
                    'user_id'        => $i ]);
        }

        DB::table('group_interest')
          ->insert(
              [ 'group_id'    => 1,
                'interest_id' => 1 ]);

        DB::table('group_interest')
          ->insert(
              [ 'group_id'    => 2,
                'interest_id' => 2 ]);

        DB::table('group_interest')
          ->insert(
              [ 'group_id'    => 2,
                'interest_id' => 3 ]);

        DB::table('posts')
          ->insert(
              [ 'text'       => "damn nice post, eh?",
                'created_by' => 1,
                'created_at' => Carbon::now() ]);

        DB::table('posts')
          ->insert(
              [ 'text'       => "another work of art!",
                'created_by' => 2,
                'created_at' => Carbon::now() ]);

        DB::table('posts')
          ->insert(
              [ 'text'       => "word",
                'created_by' => 3,
                'parent_id'  => 2,
                'created_at' => Carbon::now() ]);

        DB::table('group_post')
          ->insert(
              [ 'group_id'   => 2,
                'post_id'    => 1,
                'created_at' => Carbon::now() ]);

        DB::table('group_post')
          ->insert(
              [ 'group_id'   => 1,
                'post_id'    => 2,
                'created_at' => Carbon::now() ]);

        DB::table('group_post')
          ->insert(
              [ 'group_id'   => 1,
                'post_id'    => 3,
                'created_at' => Carbon::now() ]);

        DB::table('events')
          ->insert(
              [ 'group_id'    => 1,
                'user_id'     => 1,
                'title'       => 'Baking party',
                'description' => 'lets bake!',
                'start'       => '2016-10-21 13:00:00',
                'end'         => '2016-10-21 14:00:00',
                'created_at'  => Carbon::now() ]);

        DB::table('event_user')
          ->insert(
              [ 'event_id'   => 1,
                'user_id'    => 1,
                'vote'       => 'yes',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now() ]);

        for ( $i = 2; $i <= $user_count; $i++ ) {
            DB::table('gift_user')
              ->insert(
                  [ 'gift_id'     => random_int(1, 4),
                    'sender_id'   => 1,
                    'receiver_id' => $i, ]);
        }

        for ( $j = 2; $j <= $user_count; $j++ ) {
            DB::table('gift_user')
              ->insert(
                  [ 'gift_id'     => random_int(1, 4),
                    'sender_id'   => 2,
                    'receiver_id' => $j, ]);
        }

        DB::table('gift_user')
          ->insert(
              [ 'gift_id'     => random_int(1, 4),
                'sender_id'   => 2,
                'receiver_id' => 1, ]);
    }
}
