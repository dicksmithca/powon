<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Announcement
 *
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Announcement whereId( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Announcement whereTitle( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Announcement whereBody( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Announcement whereCreatedBy( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Announcement whereCreatedAt( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Announcement whereUpdatedAt( $value )
 * @mixin \Eloquent
 */
class Announcement extends Model {

    protected $fillable = [ 'title',
                            'body' ];

    public static function boot() {

        parent::boot();
        // first we tell the model what to do on a creating event
        static::creating(
            function ($announcement) {

                $announcement->created_by = Auth::user()->id;
            });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {

        return $this->belongsTo('App\User', 'created_by');
    }
}
