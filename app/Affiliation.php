<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Affiliation
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Query\Builder|\App\Affiliation whereId( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Affiliation whereName( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Affiliation whereCreatedAt( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Affiliation whereUpdatedAt( $value )
 * @mixin \Eloquent
 */
class Affiliation extends Model {

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users() {

        return $this->belongsToMany('App\User');
    }
}
