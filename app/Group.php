<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Group
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $owner
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Interest[] $interests
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Post[] $posts_all
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Post[] $posts_top
 * @method static \Illuminate\Database\Query\Builder|\App\Group whereId( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Group whereName( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Group whereDescription( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Group whereCreatedBy( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Group whereCreatedAt( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Group whereUpdatedAt( $value )
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Event[] $events
 */
class Group extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name',
                            'description', ];

    public static function boot() {

        parent::boot();
        // first we tell the model what to do on a creating event
        static::creating(
            function ($group) {

                $group->created_by = Auth::user()->id;
            });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner() {

        return $this->belongsTo('App\User', 'created_by');
    }

    /**
     * @return static
     */
    public function users() {

        $temp  = $this->belongsToMany('App\User')
                      ->withPivot('approved_at')
                      ->get();
        $temp1 = $temp->where('pivot.approved_at', null);

        return $temp->diff($temp1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function interests() {

        return $this->belongsToMany('App\Interest');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts_all() {

        return $this->belongsToMany('App\Post');
    }

    /**
     * @return mixed
     */
    public function posts_top() {

        $posts = $this->belongsToMany('App\Post');

        return $posts->whereNull('parent_id');

    }

    public function events() {

        return $this->hasMany('App\Event', 'group_id');
    }

}
