<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Interest
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Query\Builder|\App\Interest whereId( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Interest whereName( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Interest whereCreatedAt( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Interest whereUpdatedAt( $value )
 * @mixin \Eloquent
 */
class Interest extends Model {

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users() {

        return $this->belongsToMany('App\User');
    }
}
