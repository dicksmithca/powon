<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Content
 *
 * @property integer $id
 * @property integer $post_id
 * @property string $path
 * @property string $label
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Post $post
 * @method static \Illuminate\Database\Query\Builder|\App\Content whereId( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Content wherePostId( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Content whereUserId( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Content wherePath( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Content whereLabel( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Content whereCreatedAt( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Content whereUpdatedAt( $value )
 * @mixin \Eloquent
 */
class Content extends Model {

    public function post() {

        return $this->hasOne('App\Post');
    }
}
