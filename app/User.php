<?php

namespace App;

use Auth;
use DB;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;

/**
 * App\User
 *
 * @property integer $id
 * @property string $name_family
 * @property string $name_given
 * @property string $date_birthed
 * @property string $status
 * @property string $gender
 * @property string $address
 * @property string $email
 * @property string $password
 * @property string $profile_permissions
 * @property string $roles
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $stripe_id
 * @property string $card_brand
 * @property string $card_last_four
 * @property string $trial_ends_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Announcement[] $announcements
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Affiliation[] $affiliations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Interest[] $interests
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Group[] $groups
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Group[] $owned_groups
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $contacts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $requests
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Message[] $messages
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Conversation[] $conversations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Gift[] $gifts
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Cashier\Subscription[] $subscriptions
 * @method static \Illuminate\Database\Query\Builder|\App\User whereId( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User whereNameFamily( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User whereNameGiven( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User whereDateBirthed( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User whereStatus( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User whereGender( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User whereAddress( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmail( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePassword( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User whereProfilePermissions( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRoles( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRememberToken( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCreatedAt( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUpdatedAt( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User whereStripeId( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCardBrand( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCardLastFour( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User whereTrialEndsAt( $value )
 * @mixin \Eloquent
 * @property boolean $password_invalid
 * @property string $details_permissions
 * @property string $contacts_permissions
 * @property string $groups_permissions
 * @property string $attributes_permissions
 * @property string $feed_permissions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Event[] $owned_events
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePasswordInvalid( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User whereDetailsPermissions( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User whereContactsPermissions( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User whereGroupsPermissions( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User whereAttributesPermissions( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\User whereFeedPermissions( $value )
 */
class User extends Authenticatable {

    // For Cashier/Stripe
    use Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name_family',
                            'name_given',
                            'date_birthed',
                            'date_joined',
                            'status',
                            'gender',
                            'email',
                            'password',
                            'address',
                            'details_permissions',
                            'contacts_permissions',
                            'groups_permissions',
                            'attributes_permissions',
                            'feed_permissions', ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ 'password',
                          'remember_token', ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function announcements() {

        return $this->hasMany('App\Announcement', 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function affiliations() {

        return $this->belongsToMany('App\Affiliation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function interests() {

        return $this->belongsToMany('App\Interest');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function groupRequests() {

        $ownerof = $this->groups()
                        ->get();
        $ownerof = $ownerof->pluck('id');

        $requests = DB::table('group_user')
                      ->join('groups', 'group_user.group_id', '=', 'groups.id')
                      ->join('users', 'group_user.user_id', '=', 'users.id')
                      ->select(
                          'group_id',
                          'groups.name',
                          'users.id as user_id',
                          'users.name_given',
                          'users.name_family')
                      ->whereIn('group_id', $ownerof)
                      ->where('groups.created_by', '=', $this->id)
                      ->whereNull('approved_at')
                      ->distinct()
                      ->get();

        return collect($requests);
    }

    public function groups() {

        return $this->belongsToMany('App\Group', 'group_user')
                    ->withPivot('approved_at')
                    ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function owned_groups() {

        return $this->hasMany('App\Group', 'created_by');
    }

    public function owned_events() {

        return $this->hasMany('App\Event', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages() {

        return $this->hasMany('App\Message');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function conversations() {

        return $this->belongsToMany(
            'App\Conversation',
            'conversation_user')
                    ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function gifts() {

        return $this->belongsToMany('App\Gift');
    }

    /**
     * @return bool
     */
    public function is_same() {

        return Auth::user()->id === $this->id;
    }

    public function isContactOf($user) {

        return !$this->getUserContacts()
                     ->where('id', $user->id)
                     ->isEmpty();
    }

    /**
     * @return static
     */
    public function getUserContacts() {

        $my_requests    = $this->contacts()
                               ->get();
        $their_requests = $this->requests()
                               ->get();

        return $user_contacts = $my_requests->intersect($their_requests);
    }

    /**
     * @return $this
     */
    public function contacts() {

        return $this->belongsToMany(
            'App\User',
            'contact_user',
            'source_user_id',
            'target_user_id')
                    ->withPivot('contact_id');
    }

    /**
     * @return $this
     */
    public function requests() {

        return $this->belongsToMany(
            'App\User',
            'contact_user',
            'target_user_id',
            'source_user_id')
                    ->withPivot('contact_id');
    }

    public function isMemberOfSameGroup($user) {

        $user1_groups = $this->memberOf()
                             ->pluck('id');
        $user2_groups = $user->memberOf()
                             ->pluck('id');
        if ( $user1_groups->isEmpty() ) {
            return false;
        }
        if ( $user2_groups->isEmpty() ) {
            return false;
        }

        return $user1_groups->intersect($user2_groups);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function memberOf() {

        $memberOf = DB::table('group_user')
                      ->join('groups', 'group_user.group_id', '=', 'groups.id')
                      ->select('groups.*')
                      ->where('user_id', '=', $this->id)
                      ->whereNotNull('approved_at')
                      ->distinct()
                      ->get();

        return collect($memberOf);
    }

}
