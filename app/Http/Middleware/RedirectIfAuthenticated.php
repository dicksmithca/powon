<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class RedirectIfAuthenticated {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {

        if ( Auth::user() && Auth::user()->password_invalid
             && !$request->is(
                'password/change')
        ) {
            return redirect('password/change');
        }

        if ( Auth::user() && Auth::user()->status == 'suspended' ) {
            Auth::logout();

            return response('Forbidden.', 403);
        }

        if ( Auth::guard($guard)
                 ->check()
        ) {
            return redirect('/');
        }

        return $next($request);
    }
}
