<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Illuminate\Http\Request;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     */
    public function __construct() {

        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $user         = Auth::user();
        $user_groups  = $user->memberOf();
        $owned_groups = $user->owned_groups()
                             ->getResults();

        //Real contacts are those in both contacts and requests
        $my_requests    = $user->contacts()
                               ->get();
        $their_requests = $user->requests()
                               ->get();

        $user_contacts = $my_requests->intersect($their_requests);
        //Contact type id to string
        $user_contacts->transform(
            function ($item) {

                switch ( $item->pivot->contact_id ) {
                    case 1:
                        $item->pivot->contact_id = "Family";
                        break;
                    case 2:
                        $item->pivot->contact_id = "Friend";
                        break;
                    case 3:
                        $item->pivot->contact_id = "Colleague";
                        break;
                }

                return $item;
            });
        $user_requests = $their_requests->diff($my_requests);

        $user_interests    = $user->interests()
                                  ->get();
        $user_affiliations = $user->affiliations()
                                  ->get();

        $user_conversations = DB::table('conversation_user')
                                ->join(
                                    'conversations',
                                    'conversation_user.conversation_id',
                                    '=',
                                    'conversations.id')
                                ->select(
                                    'conversations.id',
                                    'conversations.title',
                                    'conversations.updated_at')
                                ->where(
                                    'conversation_user.user_id',
                                    '=',
                                    $user->id)
                                ->groupBy('conversations.id')
                                ->orderBy('conversations.updated_at', 'desc')
                                ->get();

        $incoming_group_requests = $user->groupRequests();

        $user_gifts = DB::table('gift_user')
                        ->select(
                            'gift_user.gift_id',
                            'gift_user.receiver_id',
                            'gift_user.sender_id')
                        ->where('gift_user.receiver_id', '=', $user->id)
                        ->join('users', 'gift_user.sender_id', '=', 'users.id')
                        ->select('name_given', 'gift_id')
                        ->get();

        $posts = $this->get_recent_top_posts(10);

        return view(
            'home',
            [ 'user'               => $user,
              'user_groups'        => $user_groups,
              'user_contacts'      => $user_contacts,
              'user_requests'      => $user_requests,
              'user_interests'     => $user_interests,
              'user_affiliations'  => $user_affiliations,
              'user_conversations' => $user_conversations,
              'group_requests'     => $incoming_group_requests,
              'user_gifts'         => $user_gifts,
              'owned_groups'       => $owned_groups,
              'posts'              => $posts ]);
    }

    // confirms that invitation e-mail was sent
    /**
     * @param $count
     * @return \Illuminate\Support\Collection
     */
    private function get_recent_top_posts($count) {

        $user            = Auth::user();
        $all_user_groups = $user->groups()
                                ->get();
        if ( count($all_user_groups) === 0 ) {
            return collect();
        }
        $all_groups_posts = $all_user_groups->first()
                                            ->posts_top()
                                            ->get();
        foreach ( $all_user_groups as $group ) {
            foreach ( $group->posts_top()
                            ->get() as $group_post ) {
                $all_groups_posts->push($group_post);
            }
        }
        $sorted_unique_groups_posts = $all_groups_posts->unique()
                                                       ->sortByDesc('id');

        return $sorted_unique_groups_posts->take($count);
    }

    /**
     * @param Request $request
     * @param $receiver
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendInvite(Request $request, $receiver) {

        return redirect()->back();
    }

    /**
     * @param $count
     * @return \Illuminate\Support\Collection
     */
    private function get_recent_posts($count) {

        $user            = Auth::user();
        $all_user_groups = $user->groups()
                                ->get();
        if ( count($all_user_groups) === 0 ) {
            return collect();
        }
        $all_groups_posts = $all_user_groups->first()
                                            ->posts_all()
                                            ->get();
        foreach ( $all_user_groups as $group ) {
            $all_groups_posts->union(
                $group->posts_all()
                      ->get());
        }
        $sorted_unique_groups_posts = $all_groups_posts->unique()
                                                       ->sortByDesc('id');

        return $sorted_unique_groups_posts->take($count);
    }
}
