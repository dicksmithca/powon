<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use DB;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Validator;

class AuthController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     */
    public function __construct() {

        $this->middleware($this->guestMiddleware(), [ 'except' => 'logout' ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {

        return Validator::make(
            $data,
            [ 'inviter_email'   => 'required|exists:users,email,email,'
                                   . $data['inviter_email'],
              'inviter_name'    => 'required|exists:users,name_given,email,'
                                   . $data['inviter_email'],
              'inviter_birthed' => 'required|exists:users,date_birthed,email,'
                                   . $data['inviter_email'],
              'name_family'     => 'required|max:255',
              'name_given'      => 'required|max:255',
              'email'           => 'required|email|max:255|unique:users',
              'password'        => 'required|min:6|confirmed',
              'date_birthed'    => 'required|date|before:tomorrow',
              'gender'          => 'required|in:female,male',
              'address'         => 'required|max:255',
              'stripeToken'     => 'required', ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     *
     * @return User
     */
    protected function create(array $data) {

        $user = User::create(
            [ 'name_family'  => $data['name_family'],
              'name_given'   => $data['name_given'],
              'email'        => $data['email'],
              'password'     => bcrypt($data['password']),
              'date_birthed' => $data['date_birthed'],
              'gender'       => $data['gender'],
              'address'      => $data['address'], ]);

        $user->newSubscription('primary', 'yearly_membership')
             ->create($data['stripeToken']);

        DB::table('contact_user')
          ->insert(
              [ 'source_user_id' => $user->id,
                'target_user_id' => User::where('email', $data['inviter_email'])
                                        ->first()->id,
                'contact_id'     => 2, ]);

        return $user;
    }
}
