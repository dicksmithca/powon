<?php

namespace App\Http\Controllers;

use App\Group;
use App\Post;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class PostController extends Controller {

    public function create(Request $request, $group_id, $parent_id = null) {

        if ( !isset( $parent_id ) ) {
            $parent_id = 0;
        }
        $text_entry_name = 'post' . $parent_id;
        $this->validate(
            $request,
            [ $text_entry_name => 'required', ]);
        $post       = new Post();
        $post->text = $request[$text_entry_name];
        if ( $parent_id != 0 ) {
            $post->parent_id = $parent_id;
        }
        Group::find($group_id)
             ->posts_all()
             ->save($post);
        if ( isset( $request['photo'] ) ) {
            $this->uploadThing($request, $post->id);
        }

        return redirect()->route('group', [ 'group_id' => $group_id ]);
    }

    private function uploadThing($request, $post_id) {

        $this->validate(
            $request,
            [ 'photo' => 'mimes:jpeg,bmp,png',
              'label' => 'required|max:255' ]);

        if ( $request->file('photo')
                     ->isValid()
        ) {
            $path = base_path('resources/img/');
            $request->file('photo')
                    ->move(
                        $path,
                        $request->file('photo')
                                ->getFilename());

            // top is for local
            // $webpath = str_replace('public/','', url('resources/img/'));
            $webpath = url('powon/resources/img/');

            DB::table('contents')
              ->insert(
                  [ 'post_id'    => $post_id,
                    'path'       => $webpath . '/' . $request->file('photo')
                                                             ->getFilename(),
                    'label'      => $request['label'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now() ]);
        }
    }

    public function delete($post_id) {

        $user = Auth::user();
        $post = Post::where('id', $post_id)
                    ->first();
        if ( ( $user->id !== $post->owner()
                                  ->first()->id )
             && ( $user->roles !== 'admin' )
        ) {
            return redirect()->route('main');
        }
        $post->delete();

        return redirect()->back();
    }

    public function edit(Request $request, $group_id, $post_id) {

        $post = Post::where('id', $post_id)
                    ->first();
        if ( ( Auth::user()->id !== $post->owner()
                                         ->first()->id )
        ) {
            return redirect()->route('main');
        }
        $text_entry_name = 'post_edit' . $post_id;
        $this->validate(
            $request,
            [ $text_entry_name => 'required', ]);
        $text       = $request[$text_entry_name];
        $post->text = $text;

        $post->save();
        if ( isset( $request['photo'] ) ) {
            $this->uploadThing($request, $post_id);
        } elseif ( isset( $request['delete_content_checkbox' . $post_id] ) ) {
            DB::table('contents')
              ->where('post_id', $post_id)
              ->delete();

            return redirect()->back();
        }

        $this->set_permissions($request, $post_id, $group_id);

        return redirect()->back();
    }

    private function set_permissions($request, $post_id, $group_id) {

        $group_users = Group::find($group_id)
                            ->users()
                            ->diff(collect([ Auth::user() ]));
        foreach ( $group_users as $user ) {
            $permission =
                $request['radio_permissions_' . $post_id . '_' . $user->id];
            $p          = DB::table('post_user')
                            ->where('post_id', $post_id)
                            ->where('user_id', $user->id);
            if ( $permission != 'read only' ) {
                if ( $p->count() == 0 ) {
                    DB::table('post_user')
                      ->insert(
                          [ 'post_id'    => $post_id,
                            'user_id'    => $user->id,
                            'permission' => $permission,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now() ]);
                } else {
                    if ( $permission != $p->first()->permission ) {
                        $p->update(
                            [ 'permission' => $permission,
                              'updated_at' => Carbon::now() ]);
                    }
                }
            } elseif ( !is_null($p) ) {
                $p->delete();
            }
        }
    }

    public function share(Request $request, $post_id, $group_id) {

        $group = Group::where('id', $group_id);
        $post  = Post::where('id', $post_id);
        $group->save($post);

        return redirect()->back();
    }

}

