<?php

namespace App\Http\Controllers;

use App\Interest;
use Illuminate\Http\Request;

class InterestController extends Controller {

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        $interests = Interest::all();

        return view(
            'interests',
            [ 'interests' => $interests ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function createInterest(Request $request) {

        $this->validate(
            $request,
            [ 'interest_name' => 'required|max:63' ]);

        $match = Interest::where('name', $request['interest_name'])
                         ->first();

        if ( !empty( $match ) ) {
            $request->session()
                    ->flash(
                        'message',
                        "Could not create interest as it already exists");

            return redirect()->back();
        }

        $interest       = new Interest();
        $interest->name = $request['interest_name'];
        $request->user()
                ->interests()
                ->save($interest);

        $interests = Interest::all();

        return view(
            'interests',
            [ 'interests' => $interests ]);
    }
}

