<?php

namespace App\Http\Controllers;

use App\Conversation;
use App\User;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class ConversationController extends Controller {

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        $user    = Auth::user();
        $user_id = $user->id;

        //Get conversations that the user is involved in
        $user_convos = DB::select(
            '
            SELECT users.name_given, users.name_family, conversations.title, conversation_id, user_id, conversation_user.created_at
            FROM conversation_user, users, conversations
            WHERE (conversation_id) IN 
            (
                SELECT conversation_id
                FROM conversation_user
                WHERE user_id = :id
            )
            AND users.id = conversation_user.user_id
            AND conversation_user.conversation_id = conversations.id
            GROUP BY conversation_id, user_id',
            [ 'id' => $user_id ]);

        $user_convos = collect($user_convos)->groupBy('conversation_id');

        //Real contacts are those in both contacts and requests
        if ( $user->roles !== 'admin' ) {
            $my_requests    = $user->contacts()
                                   ->get();
            $their_requests = $user->requests()
                                   ->get();
            $user_contacts  = $my_requests->intersect($their_requests);
        } else {
            $user_contacts = User::all();
            $admin         = User::all()
                                 ->where('id', $user_id);
            $user_contacts = $user_contacts->diff($admin);
        }

        return view(
            'conversations',
            [ 'user_contacts' => $user_contacts,
              'user_convos'   => $user_convos, ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createConversation(Request $request) {

        $this->validate(
            $request,
            [ 'title' => 'required|max:255',
              'users' => 'required', ]);

        $convo_users         = $request['users'];
        $conversation        = new Conversation();
        $conversation->title = $request['title'];
        //Create conversation

        $result   = $request->user()
                            ->conversations()
                            ->save($conversation);
        $convo_id = $result->id;

        //Add users to conversation
        foreach ( $convo_users as $convo_user ) {
            DB::table('conversation_user')
              ->insert(
                  [ 'conversation_id' => $convo_id,
                    'user_id'         => $convo_user,
                    'created_at'      => Carbon::now(),
                    'updated_at'      => Carbon::now() ]);
        }

        return redirect()->action('ConversationController@index');
    }

    /**
     * @param $conv_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($conv_id) {

        $user           = Auth::user();
        $users_in_convo = Conversation::find($conv_id)
                                      ->users()
                                      ->get();

        $verify = $users_in_convo->pluck('id')
                                 ->unique()
                                 ->all();
        if ( !in_array($user->id, $verify) ) {
            return redirect()->route('main');
        }
        $conversation = Conversation::where('id', $conv_id)
                                    ->first();
        $conversation->delete();

        return redirect()->action('ConversationController@index');
    }
}
