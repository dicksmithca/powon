<?php

namespace App\Http\Controllers;

use App\Affiliation;
use App\Interest;
use App\User;
use Auth;
use DB;
use Illuminate\Http\Request;

class UserController extends Controller {

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        $users = User::all();

        return view(
            'users',
            [ 'users' => $users, ]);
    }

    /**
     * @param Request $request
     * @param $user_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show(Request $request, $user_id) {

        $user = User::where('id', $user_id)
                    ->first();
        if ( $user->is_same() ) {
            return redirect()->action('HomeController@index');
        }
        $user_groups    = $user->groups()
                               ->get();
        $my_requests    = $user->contacts()
                               ->get();
        $their_requests = $user->requests()
                               ->get();

        $user_contacts = $my_requests->intersect($their_requests);

        $is_contact = $user_contacts->contains('id', Auth::user()->id);

        $contact_request = 0;
        if ( !$is_contact ) {
            $contact_request = DB::table('contact_user')
                                 ->select('source_user_id', 'target_user_id')
                                 ->where(
                                     'source_user_id',
                                     '=',
                                     Auth::user()->id)
                                 ->where('target_user_id', '=', $user_id)
                                 ->first();
        }

        $user_interests    = $user->interests()
                                  ->get();
        $user_affiliations = $user->affiliations()
                                  ->get();

        $owned_groups = $user->owned_groups()
                             ->get();

        return view(
            'user',
            [ 'user'              => $user,
              'user_groups'       => $user_groups,
              'user_contacts'     => $user_contacts,
              'user_interests'    => $user_interests,
              'user_affiliations' => $user_affiliations,
              'is_contact'        => $is_contact,
              'contact_request'   => $contact_request,
              'owned_groups'      => $owned_groups, ]);
    }

    /**
     * @param Request $request
     * @param $user_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function promote(Request $request, $user_id) {

        $user = User::where('id', $user_id)
                    ->first();
        /* double check to avoid bypassing via routes, make sure we're promoting
         * a non-admin member and that the change was initiated by an admin
         */
        if ( $user->roles !== 'admin' && Auth::user()->roles === 'admin' ) {
            $user->roles = 'admin';
            $user->save();
        }

        return redirect()->action(
            'UserController@show',
            [ 'user_id' => $user_id ]);
    }

    /**
     * @param Request $request
     * @param $user_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function demote(Request $request, $user_id) {

        $user = User::where('id', $user_id)
                    ->first();
        /* double check to avoid bypassing via routes, make sure we're promoting
         * a non-admin member and that the change was initiated by an admin
         */
        if ( $user->roles === 'admin' && Auth::user()->roles === 'admin' ) {
            $user->roles = 'member';
            $user->save();
        }

        return redirect()->action(
            'UserController@show',
            [ 'user_id' => $user_id ]);
    }

    /**
     * @param Request $request
     * @param $update_type
     * @param $user_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update_status(Request $request, $update_type, $user_id) {

        $user = User::where('id', $user_id)
                    ->first();
        /* double check to avoid bypassing via routes, make sure we're promoting
         * a non-admin member and that the change was initiated by an admin
         */
        if ( !( Auth::user()->roles === 'admin' ) ) {
            return redirect()->back();
        }

        // now proceed
        switch ( $update_type ) {
            case 'activate':
                $user->status = 'active';
                $user->save();
                break;

            case'deactivate':
                $user->status = 'inactive';
                $user->save();
                break;

            case 'suspend':
                $user->status = 'suspended';
                $user->save();
                break;
        }

        return redirect()->action(
            'UserController@show',
            [ 'user_id' => $user_id ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request) {

        $this->validate(
            $request,
            [ 'search_string' => 'required|max:255', ]);

        $search_string = $request['search_string'];

        // the condition to redirect back is if users is a collection with
        //  0 elements, we need to guarantee the variable is a collection first
        // as -> count won't work on an uninitialized variable and isempty doesn't
        // work on an empty collection...
        $users = collect([ ]);

        switch ( $request['filter_field'] ) {
            case "name":
                $users = User::where(
                    'name_family',
                    'like',
                    '%' . $search_string . '%')
                             ->orWhere(
                                 'name_given',
                                 'like',
                                 '%' . $search_string . '%')
                             ->get();
                break;
            case "interests":
                $interest =
                    Interest::where('name', 'like', '%' . $search_string . '%')
                            ->first();
                if ( !empty( $interest ) ) {
                    $users = $interest->users()
                                      ->get();
                }
                break;
            case "affiliations":
                $affiliation = Affiliation::where(
                    'name',
                    'like',
                    '%' . $search_string . '%')
                                          ->first();
                if ( !empty( $affiliation ) ) {
                    $users = $affiliation->users()
                                         ->get();
                }
                break;
        }

        if ( $users->isEmpty()
             || ( ( $users->count() === 1 )
                  && ( $users->first()->id === Auth::user()->id ) )
        ) {
            $request->session()
                    ->flash(
                        'message',
                        "Could not find members matching the selected criteria");
            $users = User::all();
        } else {
            $filtered_by =
                'Members filtered by ' . $request['filter_field'] . ': '
                . $search_string;
            $request->session()
                    ->flash('filtered_by', $filtered_by);
        }

        return view(
            'users',
            [ 'users' => $users, ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function settings() {

        return view(
            'settings',
            [ 'user' => Auth::user() ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request) {

        $this->validate(
            $request,
            [ 'name_family'            => 'required|max:255',
              'name_given'             => 'required|max:255',
              'email'                  => 'required|email|max:255|unique:users,email,'
                                          . Auth::user()->id,
              'date_birthed'           => 'required|date|before:tomorrow',
              'gender'                 => 'required|in:female,male',
              'address'                => 'required|max:255',
              'details_permissions'    => 'required|in:private,contacts,groups',
              'contacts_permissions'   => 'required|in:private,contacts,groups',
              'groups_permissions'     => 'required|in:private,contacts,groups',
              'attributes_permissions' => 'required|in:private,contacts,groups',
            ]);

        Auth::user()
            ->update(
                [ 'name_family'            => $request['name_family'],
                  'name_given'             => $request['name_given'],
                  'email'                  => $request['email'],
                  'date_birthed'           => $request['date_birthed'],
                  'gender'                 => $request['gender'],
                  'address'                => $request['address'],
                  'details_permissions'    => $request['details_permissions'],
                  'contacts_permissions'   => $request['contacts_permissions'],
                  'groups_permissions'     => $request['groups_permissions'],
                  'attributes_permissions' => $request['attributes_permissions'],
                ]);

        return view(
            'settings',
            [ 'user' => Auth::user() ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function password() {

        return view('auth.passwords.change', [ 'user' => Auth::user() ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function set_password(Request $request) {

        $this->validate(
            $request,
            [ 'password' => 'required|min:6|confirmed', ]);

        $user = Auth::user();

        $user->password         = bcrypt($request['password']);
        $user->password_invalid = false;
        $user->save();

        return redirect('/');
    }

    public function set_payment(Request $request) {

        $user = Auth::user();

        if ( $user->subscription()
             && $user->subscription()
                     ->active()
        ) {
            $user->subscription()
                 ->update($request['stripeToken']);
        } else {
            $user->newSubscription('primary', 'yearly_membership')
                 ->create($request['stripeToken']);
        }

        return view(
            'settings',
            [ 'user' => Auth::user() ]);
    }
}