<?php

namespace App\Http\Controllers;

use App\Announcement;
use Auth;
use Illuminate\Http\Request;

class AnnouncementController extends Controller {

    /*protected $announcements;*/

    /**
     * AnnouncementController constructor.
     */
    public function __construct() {
        /*$this->announcements = $announcements;*/
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        if ( Auth::user() && Auth::user()->password_invalid ) {
            return redirect('password/change');
        }

        if ( Auth::user() && Auth::user()->status == 'suspended' ) {
            Auth::logout();

            return response('Forbidden.', 403);
        }

        $announcements = Announcement::all();

        return view(
            'welcome',
            [ 'announcements' => $announcements, ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {

        $this->validate(
            $request,
            [ 'title' => 'required|max:255',
              'body'  => 'required' ]);

        $announcement        = new Announcement();
        $announcement->title = $request['title'];
        $announcement->body  = $request['body'];
        $request->user()
                ->announcements()
                ->save($announcement);

        //$announcement->save();

        $announcements = Announcement::all();

        return redirect()
            ->route('main')
            ->with([ 'announcements' => $announcements ]);
        /*return view('welcome',[
            'announcements' => $announcements,
        ]);*/
    }

    /**
     * @param Request $request
     * @param $ann
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $ann) {

        $this->validate(
            $request,
            [ 'announcement-title' => 'required|max:255',
              'announcement-body'  => 'required' ]);

        $announcement = Announcement::find($ann);

        $announcement->title = $request['announcement-title'];
        $announcement->body  = $request['announcement-body'];

        $announcement->save();

        return redirect()->action('AnnouncementController@index');
    }

    /**
     * @param Request $request
     * @param $ann
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $ann) {

        $announcement = Announcement::where('id', $ann)
                                    ->first();
        $announcement->delete();
        $announcements = Announcement::all();

        return redirect()
            ->route('main')
            ->with([ 'announcements' => $announcements ]);
    }
}
