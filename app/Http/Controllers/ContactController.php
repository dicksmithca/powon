<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Illuminate\Http\Request;

class ContactController extends Controller {

    /**
     * @param Request $request
     * @param $contact_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function requestContact(Request $request, $contact_id) {

        $user_id = Auth::user()->id;

        switch ( $request['relation'] ) {
            case "family":
                $relation = 1;
                break;
            case "friend":
                $relation = 2;
                break;
            case "colleague":
                $relation = 3;
                break;
            default:
                $relation = 3;
        }

        DB::table('contact_user')
          ->insert(
              [ 'source_user_id' => $user_id,
                'target_user_id' => $contact_id,
                'contact_id'     => $relation, ]);

        return redirect()->back();

    }
}
