<?php

namespace App\Http\Controllers;

use App\Conversation;
use App\Message;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class MessageController extends Controller {

    //Has many through
    /**
     * @param $conv_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function listMessage($conv_id) {

        $user           = Auth::user();
        $users_in_convo = Conversation::find($conv_id)
                                      ->users()
                                      ->get();

        $verify = $users_in_convo->pluck('id')
                                 ->unique()
                                 ->all();
        if ( !in_array($user->id, $verify) ) {
            return redirect()->route('main');
        }

        $convo_owner = DB::table('conversations')
                         ->select(
                             'conversations.title',
                             'conversations.created_by')
                         ->where('id', '=', $conv_id)
                         ->first();

        $messages = DB::table('messages')
                      ->join('users', 'messages.user_id', '=', 'users.id')
                      ->select(
                          'users.id',
                          'users.name_given',
                          'users.name_family',
                          'messages.body',
                          'messages.created_at')
                      ->where('messages.conversation_id', '=', $conv_id)
                      ->orderBy('created_at', 'asc')
                      ->get();

        return view(
            'conversation',
            [ 'messages'       => collect($messages),
              'users_in_convo' => $users_in_convo,
              'conv_id'        => $conv_id,
              'convo_owner'    => $convo_owner, ]);
    }

    /**
     * @param Request $request
     * @param $conv_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createMessage(Request $request, $conv_id) {

        $this->validate(
            $request,
            [ 'body' => 'required', ]);

        $message                  = new Message();
        $message->conversation_id = $conv_id;
        $message->body            = $request['body'];
        $request->user()
                ->messages()
                ->save($message);

        $conversation             = Conversation::where('id', $conv_id)
                                                ->first();
        $conversation->updated_by = Auth::user()->id;
        $conversation->updated_at = Carbon::now();
        $conversation->save();

        return redirect()->back();

    }
}
