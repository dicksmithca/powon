<?php

namespace App\Http\Controllers;

use App\Event;
use App\Group;
use App\User;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class EventController extends Controller {

    public function index($group_id) {

        $events     = Event::where('group_id', $group_id)
                           ->get();
        $group      = Group::where('id', $group_id)
                           ->first();
        $group_name = $group->name;

        $group_users = $group->users();
        $is_member   = !$group_users->where('pivot.user_id', Auth::user()->id)
                                    ->isEmpty();

        return view(
            'events',
            [ 'events'     => $events,
              'group_id'   => $group_id,
              'group_name' => $group_name,
              'is_member'  => $is_member ]);
    }

    public function create(Request $request, $group_id) {

        $this->validate(
            $request,
            [ 'title'       => 'required|max:63',
              'description' => 'required|max:1023',
              'start'       => 'required|date|after:tomorrow',
              'end'         => 'required|date|after:start' ]);

        $user_id = Auth::user()->id;

        $event = new Event;

        $event->group_id    = $group_id;
        $event->user_id     = $user_id;
        $event->title       = $request['title'];
        $event->description = $request['description'];
        $event->start       = $request['start'];
        $event->end         = $request['end'];

        if ( $event->save() ) {
            DB::table('event_user')
              ->insert(
                  [ 'event_id'   => $event->id,
                    'user_id'    => $user_id,
                    'vote'       => 'yes',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(), ]);
        }

        return redirect()->back();
    }

    public function show($event_id) {

        $event     = Event::where('id', $event_id)
                          ->first();
        $group_id  = $event->group_id;
        $group     = Group::where('id', $group_id)
                          ->first();
        $owner_id  = $event->user_id;
        $owner     = User::where('id', $owner_id)
                         ->first();
        $responded = DB::table('event_user')
                       ->where('event_id', $event_id)
                       ->get();

        $respondedUsers = DB::table('event_user')
                            ->join(
                                'events',
                                'event_user.event_id',
                                '=',
                                'events.id')
                            ->join(
                                'users',
                                'event_user.user_id',
                                '=',
                                'users.id')
                            ->where('event_user.event_id', '=', $event_id)
                            ->get();

        $userRespondedAlready = true;
        $current_user_id      = Auth::user()->id;
        $myuser               = DB::table('event_user')
                                  ->where('user_id', $current_user_id)
                                  ->first();
        if ( $myuser === null ) {
            $userRespondedAlready = false;
        }

        $group_users = $group->users();
        $is_member   = !$group_users->where('pivot.user_id', Auth::user()->id)
                                    ->isEmpty();

        return view(
            'event',
            [ 'event'                => $event,
              'group'                => $group,
              'owner'                => $owner,
              'respondedUsers'       => $respondedUsers,
              'userRespondedAlready' => $userRespondedAlready,
              'is_member'            => $is_member ]);
    }

    public function response(Request $request, $event_id) {

        DB::table('event_user')
          ->insert(
              [ 'event_id'   => $event_id,
                'user_id'    => Auth::user()->id,
                'vote'       => $request['attending'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(), ]);

        return redirect()->back();
    }

}