<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class GiftController extends Controller {

    /**
     * @param Request $request
     * @param $receiver
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendGift(Request $request, $receiver) {

        // Make sure that tuple is not in the DB already
        $validation = DB::table('gift_user')
                        ->select('gift_id', 'receiver_id', 'sender_id')
                        ->where('gift_id', '=', $request['gift_name'])
                        ->where('receiver_id', '=', $receiver)
                        ->where('sender_id', '=', Auth::user()->id)
                        ->first();

        if ( is_null($validation) ) {
            DB::table('gift_user')
              ->insert(
                  [ 'gift_id'     => $request['gift_name'],
                    'receiver_id' => $receiver,
                    'sender_id'   => Auth::user()->id,
                    'created_at'  => Carbon::now(),
                    'updated_at'  => Carbon::now(), ]);
            $request->session()
                    ->flash('message', 'Gift Sent');
        } else {
            $request->session()
                    ->flash('message', 'Gift Already Sent');
        }

        return redirect()->back();
    }
}