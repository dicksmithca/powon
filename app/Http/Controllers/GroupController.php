<?php

namespace App\Http\Controllers;

use App\Group;
use App\Interest;
use App\User;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class GroupController extends Controller {

    /**
     * GroupController constructor.
     */
    public function __construct() {
        /*$this->announcements = $announcements;*/
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        $groups = Group::all();

        return view(
            'groups',
            [ 'groups' => $groups, ]);
    }

    /**
     * @param $group_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($group_id) {

        $group = Group::where('id', $group_id)
                      ->first();
        $posts = $group->posts_top()
                       ->get();
        $owner = DB::table('users')
                   ->where('id', '=', $group->created_by)
                   ->first();

        $group_users = $group->users();
        $is_member   = !$group_users->where('pivot.user_id', Auth::user()->id)
                                    ->isEmpty();

        $is_pending = collect(
            DB::table('group_user')
              ->where('user_id', '=', Auth::user()->id)
              ->where('group_id', '=', $group_id)
              ->whereNull('approved_at')
              ->get());

        $is_pending = !$is_pending->isEmpty();

        $group_interests = $group->interests()
                                 ->get();

        $interests = Interest::all();

        //Section for owner to add contacts
        $user_contacts         = Auth::user()
                                     ->getUserContacts();
        $contacts_not_in_group = $user_contacts->diff($group_users);

        if ( Auth::user()->roles == 'admin' ) {
            $users_addable = User::all()
                                 ->diff($group_users);
        } else {
            $users_addable = $contacts_not_in_group;
        }

        $other_group_users = $group_users->diff(collect([ Auth::user() ]));

        return view(
            'group',
            [ 'group'                      => $group,
              'posts'                      => $posts,
              'owner'                      => $owner,
              'group_users'                => $group_users,
              'group_interests'            => $group_interests,
              'is_member'                  => $is_member,
              'is_pending'                 => $is_pending,
              'interests'                  => $interests,
              'user_contacts_not_in_group' => $contacts_not_in_group,
              'other_group_users'          => $other_group_users,
              'users_addable'              => $users_addable ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(Request $request) {

        $this->validate(
            $request,
            [ 'name'        => 'required|max:63',
              'description' => 'required|max:511' ]);

        $group              = new Group();
        $group->name        = $request['name'];
        $group->description = $request['description'];
        $request->user()
                ->groups()
                ->save($group, [ 'approved_at' => Carbon::now() ]);

        $groups = Group::all();

        return redirect()
            ->route('groups')
            ->with([ 'groups' => $groups ]);
    }

    /**
     * @param Request $request
     * @param $group
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request, $group) {

        $group = Group::where('id', $group)
                      ->first();
        $group->delete();
        $groups = Group::all();

        return redirect()
            ->route('groups')
            ->with([ 'groups' => $groups ]);
    }

    /**
     * @param $group_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function requestMembership($group_id) {

        $user_id = Auth::user()->id;

        $already_requested = DB::table('group_user')
                               ->select('group_id', 'user_id')
                               ->where('group_id', '=', $group_id)
                               ->where('user_id', '=', $user_id)
                               ->first();

        if ( is_null($already_requested) ) {
            DB::table('group_user')
              ->insert(
                  [ 'group_id'   => $group_id,
                    'user_id'    => $user_id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(), ]);
        }

        return redirect()->back();
    }

    /**
     * @param $group_id
     * @param $user_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function acceptUser($group_id, $user_id) {

        $group = Group::where('id', $group_id)
                      ->first();

        if ( Auth::user()->id === $group->created_by ) {
            DB::table('group_user')
              ->where('group_id', '=', $group_id)
              ->where('user_id', '=', $user_id)
              ->update(
                  [ 'approved_at' => Carbon::now(), ]);
        }

        return redirect()->route('home');
    }

    /**
     * @param $group_id
     * @param $user_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function withdraw($group_id, $user_id) {

        $group = Group::where('id', $group_id)
                      ->first();

        if ( Auth::user()->id == $user_id
             || Auth::user()->id === $group->created_by
        ) {
            DB::table('group_user')
              ->where('group_id', '=', $group_id)
              ->where('user_id', '=', $user_id)
              ->delete();
        }

        if ( Auth::user()->id === $group->created_by ) {
            return redirect()->route('group', [ $group_id ]);
        }

        return redirect()->route('groups');
    }

    /**
     * @param Request $request
     * @param $group_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateGroup(Request $request, $group_id) {

        $this->validate(
            $request,
            [ 'group-nameupdate'      => 'required|max:255',
              'group-bodyupdate'      => 'required',
              'interests_update' => 'required' ]);

        $group = Group::where('id', $group_id)
                      ->first();
        if ( Auth::user()->id === $group->created_by || Auth::user()->roles ==='admin') {
            $interests = $request['interests_update'];
            $group->name        = $request['group-nameupdate'];
            $group->description = $request['group-bodyupdate'];

            $group->save();

            DB::table('group_interest')
              ->where('group_id', '=', $group_id)
              ->delete();

            foreach ( $interests as $interest ) {
                DB::table('group_interest')
                  ->insert(
                      [ 'group_id'    => $group_id,
                        'interest_id' => $interest,
                        'created_at'  => Carbon::now(),
                        'updated_at'  => Carbon::now() ]);
            }
        }

        return redirect()->route('group', [ $group_id ]);
    }

    /**
     * @param Request $request
     * @param $group_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addUsersToGroup(Request $request, $group_id) {

        $this->validate(
            $request,
            [ 'users_add'      => 'required' ]);

        $group = Group::where('id', $group_id)
                      ->first();

        if ( Auth::user()->id === $group->created_by
             || Auth::user()->roles === 'admin'
        ) {

            $users = $request['users_add'];
            foreach ( $users as $user ) {
                if ( collect(
                    DB::table('group_user')
                      ->where('user_id', $user)
                      ->where('group_id', $group_id)
                      ->first())->isEmpty()
                ) {

                    DB::table('group_user')
                      ->insert(
                          [ 'group_id'    => $group_id,
                            'user_id'     => $user,
                            'approved_at' => Carbon::now(),
                            'created_at'  => Carbon::now(),
                            'updated_at'  => Carbon::now() ]);
                } else {
                    DB::table('group_user')
                      ->where('group_id', '=', $group_id)
                      ->where('user_id', '=', $user)
                      ->update(
                          [ 'approved_at' => Carbon::now(), ]);
                }
            }
        }

        return redirect()->route('group', [ $group_id ]);
    }
}