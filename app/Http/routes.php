<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get(
    '/',
    [ 'as'   => 'main',
      'uses' => 'AnnouncementController@index' ]);

Route::auth();


/*----------Announcements--------*/
Route::get('/anns', 'AnnouncementController@index');
Route::post('/ann', 'AnnouncementController@store');
Route::post('/ann/{ann}', 'AnnouncementController@update');
Route::delete('/ann/{ann}', 'AnnouncementController@destroy');

Route::group(
    [ 'middleware' => 'auth' ],
    function () {

        /*----------Home--------*/
        Route::get(
            '/home',
            [ 'as'   => 'home',
              'uses' => 'HomeController@index' ]);
        Route::post(
            'contact/{contact_id}',
            [ 'uses' => 'ContactController@requestContact' ]);

        /*----------Conversations------------*/
        Route::get(
            '/conversations',
            [ 'as'   => 'conversations',
              'uses' => 'ConversationController@index' ]);
        Route::post(
            '/conversation',
            [ 'as'   => 'create_convo',
              'uses' => 'ConversationController@createConversation' ]);
        Route::delete(
            '/conversation/{conv_id}',
            [ 'as'   => 'delete_convo',
              'uses' => 'ConversationController@delete' ]);

        /*----------Messages------------*/
        Route::get(
            '/conversation/{conv_id}',
            [ 'uses' => 'MessageController@listMessage' ]);
        Route::post(
            '/conversation/{conv_id}/new_message',
            [ 'uses' => 'MessageController@createMessage' ]);

        /*----------Groups List--------*/
        Route::get(
            '/groups',
            [ 'as'   => 'groups',
              'uses' => 'GroupController@index' ]);
        Route::post('/group', 'GroupController@create');
        Route::delete('/groups/{group}', 'GroupController@delete');
        Route::get(
            '/groups/{group_id}',
            [ 'as'   => 'group',
              'uses' => 'GroupController@show' ]);
        Route::post(
            'groups/{group_id}/request',
            [ 'as'   => 'requestGroup',
              'uses' => 'GroupController@requestMembership' ]);
        Route::post(
            'groups/{group_id}/{user_id}',
            'GroupController@acceptUser');
        Route::delete(
            'groups/{group_id}/{user_id}',
            'GroupController@withdraw');
        Route::post('update/group/{group_id}', 'GroupController@updateGroup');
        Route::post('usr/group/{group_id}', 'GroupController@addUsersToGroup');

        /*----------Events------------*/
        Route::get(
            '/events/groups/{group_id}',
            [ 'as'   => 'events',
              'uses' => 'EventController@index' ]);
        Route::post(
            '/events/groups/{group_id}',
            [ 'as'   => 'create_event',
              'uses' => 'EventController@create' ]);
        Route::get(
            '/events/{event_id}',
            [ 'as'   => 'event',
              'uses' => 'EventController@show' ]);
        Route::post('/events/{event_id}', 'EventController@response');

        /*----------Posts-------------*/
        Route::post('/post/{group_id}/{parent_id?}', 'PostController@create');
        Route::delete(
            '/post/{post_id}',
            [ 'as'   => 'delete_post',
              'uses' => 'PostController@delete' ]);
        Route::post('/edit/post/{group_id}/{post_id}', 'PostController@edit');
        Route::post(
            '/post/share/{post_id}/{group_id}',
            [ 'as'   => 'share_post',
              'uses' => 'PostController@share' ]);

        /*----------People--------*/
        Route::get('/users', 'UserController@index');
        Route::get('/users/{user_id}', 'UserController@show');
        Route::post('/user/promote/{user_id}', 'UserController@promote');
        Route::post('/user/demote/{user_id}', 'UserController@demote');
        Route::post(
            '/user/{update_type}/{user_id}',
            'UserController@update_status');
        Route::post('/users/search/', 'UserController@search');
        Route::get('/settings', 'UserController@settings@settings');
        Route::post('/settings', 'UserController@update');
        Route::get('/password/change', 'UserController@password');
        Route::post('/password/change', 'UserController@set_password');
        Route::post('/payment', 'UserController@set_payment');

        /*-------Interests--------*/
        Route::get('/interests', 'InterestController@index');
        Route::post(
            '/interests/create',
            [ 'as'   => 'create_interest',
              'uses' => 'InterestController@createInterest' ]);

        /*-------Gifts--------*/
        Route::post(
            '/gifts/send/{user_id}',
            [ 'as'   => 'send_gift',
              'uses' => 'GiftController@sendGift' ]);

    });

// For cashier
Route::post(
    'stripe/webhook',
    '\Laravel\Cashier\Http\Controllers\WebhookController@handleWebhook');
