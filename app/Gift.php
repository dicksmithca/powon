<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Gift
 *
 * @property integer $id
 * @property string $gift_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Gift[] $sender
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Gift[] $receiver
 * @method static \Illuminate\Database\Query\Builder|\App\Gift whereId( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Gift whereGiftName( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Gift whereCreatedAt( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Gift whereUpdatedAt( $value )
 * @mixin \Eloquent
 */
class Gift extends Model {

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sender() {

        return $this->belongsToMany(
            'App\Gift',
            'gift_user',
            'gift_id',
            'sender_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function receiver() {

        return $this->belongsToMany(
            'App\Gift',
            'gift_user',
            'gift_id',
            'receiver_id');
    }
}
