<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Contact
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Contact whereId( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Contact whereName( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Contact whereCreatedAt( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Contact whereUpdatedAt( $value )
 * @mixin \Eloquent
 */
class Contact extends Model {

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function invited() {

        return $this->belongsToMany(
            'App\Contact',
            'contact_user',
            'target_user_id',
            'contact_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function requested() {

        return $this->belongsToMany(
            'App\Contact',
            'contact_user',
            'source_user_id',
            'contact_id');
    }
}
