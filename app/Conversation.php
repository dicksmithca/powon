<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Conversation
 *
 * @property integer $id
 * @property string $title
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Message[] $messages
 * @method static \Illuminate\Database\Query\Builder|\App\Conversation whereId( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Conversation whereTitle( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Conversation whereCreatedBy( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Conversation whereUpdatedBy( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Conversation whereCreatedAt( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Conversation whereUpdatedAt( $value )
 * @mixin \Eloquent
 */
class Conversation extends Model {

    public static function boot() {

        parent::boot();
        // first we tell the model what to do on a creating event
        static::creating(
            function ($conversation) {

                $conversation->created_by = Auth::user()->id;
                $conversation->updated_by = Auth::user()->id;
            });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users() {

        return $this->belongsToMany('App\User', 'conversation_user')
                    ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages() {

        return $this->hasMany('App\Message');
    }
}
