<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * App\Post
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $created_by
 * @property string $text
 * @property string $share_info
 * @property string $permissions
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $date_expired
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Group[] $groups
 * @property-read \App\User $owner
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Post[] $children
 * @property-read \App\Post $parent
 * @property-read \App\Content $content
 * @method static \Illuminate\Database\Query\Builder|\App\Post whereId( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Post whereParentId( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Post whereCreatedBy( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Post whereText( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Post whereShareInfo( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Post wherePermissions( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Post whereCreatedAt( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Post whereUpdatedAt( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Post whereDateExpired( $value )
 * @mixin \Eloquent
 */
class Post extends Model {

    public static function boot() {

        parent::boot();
        // first we tell the model what to do on a creating event
        static::creating(
            function ($post) {

                $post->created_by = Auth::user()->id;
            });
    }

    public function groups() {

        return $this->belongsToMany(
            'App\Group',
            'group_post',
            'post_id',
            'group_id');
    }

    public function owner() {

        return $this->belongsTo('App\User', 'created_by');
    }

    public function children() {

        return $this->hasMany('App\Post', 'parent_id');
    }

    public function parent() {

        return $this->hasOne('App\Post', 'parent_id');
    }

    public function super_parent() {

        $iter = $this;
        do {
            $iter = Post::find('id', $iter->parent_id);
        } while ( !is_null($iter->parent_id) );

        return $iter;
    }

    public function is_child_of($post_id) {

        $is_parent = false;
        $iter      = $this;
        do {
            if ( $iter->parent_id === $post_id ) {
                $is_parent = true;
            }
            $iter = Post::find('id', $iter->parent_id);
        } while ( !is_null($iter->parent_id) );

        return $is_parent;
    }

    public function content() {

        return $this->hasOne('App\Content');
    }

    public function has_content() {

        $content = DB::table('contents')
                     ->where('post_id', $this->id)
                     ->first();
        if ( count($content) > 0 ) {
            return $content;
        } else {
            return false;
        }
    }

    public function can_comment($user_id) {

        $permission = DB::table('post_user')
                        ->where('post_id', $this->id)
                        ->where('user_id', $user_id)
                        ->value('permission');

        return ( ( $user_id == $this->created_by )
                 || ( $permission == 'comment' )
                 || ( $permission == 'share' ) );
    }

    public function can_share($user_id) {

        $permission = DB::table('post_user')
                        ->where('post_id', $this->id)
                        ->where('user_id', $user_id)
                        ->value('permission');

        return ( $permission == 'share' );
    }

    public function permissions_to_string($user_id) {

        $str_permissions = 'r';
        if ( can_share($user_id) ) {
            $str_permissions .= ' c s';
        } elseif ( can_comment($user_id) ) {
            $str_permissions .= ' c';
        }

        return $str_permissions;
    }

}
