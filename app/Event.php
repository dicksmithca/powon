<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Event
 *
 * @property integer $id
 * @property integer $group_id
 * @property integer $user_id
 * @property string $title
 * @property string $description
 * @property string $start
 * @property string $end
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereId( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereGroupId( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereUserId( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereTitle( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereDescription( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereStart( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereEnd( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereCreatedAt( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereUpdatedAt( $value )
 * @mixin \Eloquent
 * @property-read \App\User $owner
 * @property-read \App\Group $group
 */
class Event extends Model {

    protected $fillable = [ 'title',
                            'description',
                            'start',
                            'end' ];

    public function owner() {

        return $this->belongsTo('App\User', 'user_id');
    }

    public function group() {

        return $this->belongsTo('App\Group', 'group_id');
    }
}
